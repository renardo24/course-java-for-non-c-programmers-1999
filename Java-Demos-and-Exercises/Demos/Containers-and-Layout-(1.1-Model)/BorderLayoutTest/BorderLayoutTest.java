import java.applet.*;
import java.awt.*;
import java.awt.event.*;

// Single container with border layout

public class BorderLayoutTest extends Applet implements ActionListener
{

	public void init()
	{
		Button b1, b2, b3, b4, b5;

		setBackground(Color.white);

		// Change layout to border layout

		setLayout(new BorderLayout());

		add("North", b1 = new Button("Red"));
		add("East", b2 = new Button("Green"));
		add("South", b3 = new Button("Blue"));
		add("West", b4 = new Button("White"));
		//add("Center", b5 = new Button("Black"));

		b1.addActionListener(this);
		b2.addActionListener(this);
		b3.addActionListener(this);
		b4.addActionListener(this);
		//b5.addActionListener(this);

	}


	public void actionPerformed(ActionEvent evt)
	{
		changeColor(evt.getActionCommand());
	}



	private void changeColor(String s)
	{
		if (s.equals("Red")) setBackground(Color.red);
		else if (s.equals("Green")) setBackground(Color.green);
		else if (s.equals("Blue")) setBackground(Color.blue);
		else if (s.equals("White")) setBackground(Color.white);
		else if (s.equals("Black")) setBackground(Color.black);
		repaint();
	}

}
