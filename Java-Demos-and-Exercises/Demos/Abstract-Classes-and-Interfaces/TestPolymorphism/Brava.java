public class Brava extends Car
{
	public Brava(int d, Engine e)
	{
		super(d, e);
	}


	public void selectGear(int g)
	{
		gear = g;
	}


	public int getSpeed()
	{
		return (engine.getRevs()/100) * gear;
	}


	public String toString()
	{
	  return getClass().getName();
	}

}
