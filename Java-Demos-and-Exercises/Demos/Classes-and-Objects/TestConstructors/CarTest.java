public class CarTest
{
	public static void main(String[] args)
	{
		Car car1 = new Car();
		Car car2 = new Car("Audi");
		Car car3 = new Car("BMW");

		System.out.println();
		System.out.println("car1 is a " + car1.getModel());
		System.out.println("car2 is a " + car2.getModel());
		System.out.println("car3 is a " + car3.getModel());
	}

}
