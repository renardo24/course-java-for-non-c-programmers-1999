import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import com.sun.java.swing.*;

// now uses JFC

public class RandomNumberGenerator extends JApplet implements ActionListener
{
	private JButton generate;
	private JLabel random;

	public void init()
	{
	    Container cp = getContentPane();
	    cp.setLayout(new FlowLayout());
	    
		cp.add(new JLabel("Random Number Generator"));
		cp.add(generate = new JButton("Generate"));
		cp.add(random = new JLabel("         "));
		
		generate.addActionListener(this);
	}


	public void actionPerformed(ActionEvent evt)
	{
		if (evt.getSource() == generate)
		{
			int randomNum = (int)(10000 * Math.random());
			random.setText("" + randomNum);
		}
	}
 
}