import java.awt.*;
import java.awt.event.*;


// Display an empty frame window - now converted to use Java 1.1 event model with inner class, so
// Now uses anonymous inner class, so no need to implement WindowListener interface

public class SimpleFrameWithInnerClass extends Frame
{
	public SimpleFrameWithInnerClass()
	{
		setSize(300, 200);        // resize() has been deprecated
		show();

		// register as a window listener
		addWindowListener(new WindowAdapter() {
			// must handle window-closing event to allow user to quit application
			public void windowClosing(WindowEvent we)
			{
				System.exit(0);
			}

			// no need to override any other methods
		});

	}


	public static void main(String[] args)
	{
		Frame f = new SimpleFrameWithInnerClass();
	}

}
