import java.io.*;

// Converted to Java 1.1 so now uses FileReader and LineNumberReader classes

public class LineNumberReaderTest
{

	public static void main(String[] args) throws IOException
	{
		if (args.length != 2)
		{
			System.out.println("Usage: LineNumberReaderTest "  +
									"<file name> <character>");
			System.exit(0);
		}

		FileReader inFile = new FileReader(args[0]);
		LineNumberReader in = new LineNumberReader(inFile);
		int searchValue = args[1].charAt(0);

		int ch;		
		while ((ch = in.read()) != -1)
		{
			if (ch == searchValue)
			{
				System.out.println("Found character " + (char)searchValue +
									" at line " + (in.getLineNumber() +1));
				break;
			}
		}
		in.close();
	}

}

