import java.applet.*;
import java.awt.*;

public class RadioTest extends Applet
{
	SwitchPanel switchPanel;
	Radio radio;

    public void init()
    {
		setLayout(new BorderLayout());

		AudioClip ac = getAudioClip(getDocumentBase(), "spacemusic.au");
		radio = new Radio(ac);
		switchPanel = new SwitchPanel(radio);

		add("South", switchPanel);
		add("Center", radio);
	}

}

