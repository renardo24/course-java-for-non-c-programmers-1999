public class Car
{
	private int currentGear;

	void selectGear(byte g)
	{
		if (g > 4)
		{
			System.out.println("Too high a gear - setting gear to 4");
			g = 4;
			currentGear = g;
		}
	}

}

