import java.awt.*;

public class LoginDialog extends Dialog
{
	DialogProcessor parent;
	TextField name, password;

	public LoginDialog(DialogProcessor parent)
	{
		super((Frame)parent, "Login", true);  // modal
		this.parent = parent;
		setLayout(new FlowLayout());
		add(new Label("Name"));
		add( name = new TextField(12));
		add(new Label("Password"));
		add(password = new TextField(12));
		password.setEchoCharacter('*');
		add(new Button("OK"));
		add(new Button("Cancel"));
		pack();
	}

	public boolean action(Event evt, Object arg)
	{
		Dialog dialog;
		if (arg.equals("OK"))
		{
			dispose();
			LoginDialogInfo info = new LoginDialogInfo(
						name.getText(), password.getText());
			parent.processDialog(this, info);
			return true;
		}
		else if (arg.equals("Cancel"))
		{
			dispose();
			return true;
		}
		return false;
	}

	
	public boolean handleEvent(Event evt)
	{
		if (evt.id == Event.WINDOW_DESTROY)
		{
			dispose();
			return true;
		}
		return super.handleEvent(evt);
	}


}
