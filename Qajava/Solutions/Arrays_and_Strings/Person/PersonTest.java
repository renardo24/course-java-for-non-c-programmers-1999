/* PersonTest is a Java application for testing the Person class */

public class PersonTest
{

	// Class variables to hold sample test values
	//
	private static String[] testNames = {"Hill", "Diniz", "Villeneuve",
											"Frentzen", "Schumacher", "Irvine",
											"Alesi", "Berger", "Hakkinen",
											"Panis", "Herbert", "Larini" };
	private static int[] testAges = {36, 26, 25, 29, 28, 31, 32, 37, 28, 30, 32, 32};


	//
	// The main() method is the entry point of this class
	//
	public static void main(String[] args)
	{
		// Declare a variable capable of referencing an array of persons
		//
		Person[] persons;

		// Create an array capable of referencing some number of persons
		//
		int count = testNames.length;
		persons = new Person[count];

		// Fill array with references to Person objects
		//
		for (int i = 0; i < persons.length; i++)
			persons[i] = new Person(testNames[i], testAges[i]);

		// Print initial contents of array
		//
		System.out.println("\nThe original list of people is:\n");
		for (int i = 0; i < persons.length; i++)
			System.out.println("  " + persons[i].getName() + "   \t" + persons[i].getAge());
		
		// Sort array so that names are in age order
		//
		//Person.bSortByAge(persons);

		// Sort array so that names are in alphabetical order
		Person.bSortByName(persons);

		// Print sorted contents of array
		//
		System.out.println("\nThe sorted list of people is:\n");
		for (int i = 0; i < persons.length; i++)
			System.out.println("  " + persons[i].getName() + "   \t" + persons[i].getAge());


	}

}