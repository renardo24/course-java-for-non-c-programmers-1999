import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import com.sun.java.swing.*;

// Select background colour via push buttons.
// Straight port of AWT version to JFC.

public class ColourTestButtons extends JApplet implements ActionListener
{
	public void init()
	{
		JButton b1, b2, b3, b4, b5;
		Container cp = getContentPane();
		cp.setBackground(Color.white);
		cp.setLayout(new FlowLayout(FlowLayout.CENTER));

		cp.add(b1 = new JButton("Red"));
		cp.add(b2 = new JButton("Green"));
		cp.add(b3 = new JButton("Blue"));
		cp.add(b4 = new JButton("White"));
		cp.add(b5 = new JButton("Black"));

        b1.setMnemonic('r');
        b2.setMnemonic('g');
        b3.setMnemonic('b');
        b4.setMnemonic('w');
        b5.setMnemonic('k');
        
		b1.addActionListener(this);
		b2.addActionListener(this);
		b3.addActionListener(this);
		b4.addActionListener(this);
		b5.addActionListener(this);
		
	}


	public void actionPerformed(ActionEvent evt)
	{
		changeColor(evt.getActionCommand());
	}


	private void changeColor(String s)
	{
	    Container cp = getContentPane();
		if (s.equals("Red")) cp.setBackground(Color.red);
		else if (s.equals("Green")) cp.setBackground(Color.green);
		else if (s.equals("Blue")) cp.setBackground(Color.blue);
		else if (s.equals("White")) cp.setBackground(Color.white);
		else if (s.equals("Black")) cp.setBackground(Color.black);
		repaint();
	}

}
