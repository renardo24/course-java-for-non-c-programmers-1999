import java.applet.*;
import java.awt.*;
import java.awt.event.*;

public class ColourTestButtonsWithInnerClasses extends Applet
{

	public void init()
	{
		Button b;

		setBackground(Color.white);

		add(b = new Button("Red"));
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt)
			{
				setBackground(Color.red);
				repaint();
			}
		});

		add(b = new Button("Green"));
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt)
			{
				setBackground(Color.green);
				repaint();
			}
		});

		
		add(b = new Button("Blue"));
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt)
			{
				setBackground(Color.blue);
				repaint();
			}
		});


		add(b = new Button("White"));
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt)
			{
				setBackground(Color.white);
				repaint();
			}
		});


		add(b = new Button("Black"));
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt)
			{
				setBackground(Color.black);
				repaint();
			}
		});

	}


}
