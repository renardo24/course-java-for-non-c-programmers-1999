import java.io.*;

// This version filters System.in with a DataInputStream,
// which provides a readLine() method

public class PrintStreamTest2
{

	public static void main(String[] args) throws IOException
	{
		DataInputStream in = new DataInputStream(System.in);
		PrintStream out = System.out;
		StringBuffer sb = new StringBuffer(128);
		String s;

		if (args.length == 1)
			out = new PrintStream(new FileOutputStream(args[0]));

		byte buffer[] = new byte[128];

		// read in a line of text
		while (true)
		{
			s = in.readLine();
			if (s != null)
				sb.append(s + '\n');
			else
				break;
		}
		
		if (sb.length() != 0)
		{
			s = sb.toString();
			out.println(s);
		}
		out.close();
	}

}

