public class Radio
{
	private String type;
	private String model;
	private int volume;


	public Radio(String type, String model)
	{
		this.type = type;
		this.model = model;
	}


	public int getVolume()
	{
		return volume;
	}


	public void setVolume(int volume)
	{
		this.volume = volume;
	}


}