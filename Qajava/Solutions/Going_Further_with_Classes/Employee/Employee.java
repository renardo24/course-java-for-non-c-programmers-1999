/* Class for employee */
 
public class Employee
{
	// Instance variables to hold employee's last name and age
	private String lastName;
    private int age;

	// Another instance variable to hold employee's first name
	private String firstName = "";

	// A class variable for retirement age
	private static int retirementAge = 65;

	//
	// Constructor that takes a String and an int argument
	// to initialise the instance variables for the employee's
	// last name and age, respectively.
	//
    public Employee(String ln, int a)
    {
		lastName = ln;
		age  = a;
	}


	//
	// Second constructor takes two Strings and an int argument
	// to initialise the instance variables for the employee's
	// first name, last name and age respectively
	//
    public Employee(String fn, String ln, int a)
    {
		// make use of existing constructor
		this(ln, a);
		firstName = fn;
	}


	//
	// The getName method now returns the employee's full name
	//
    public String getName()
    {
		return firstName + " " + lastName;
	}

	
	//
	// This changeName method changes the employee's last name
	//
    public void changeName(String newLastName)
    {
		lastName = newLastName;
	}


	//
	// This changeName method changes the employee's full name
	//
    public void changeName(String newFirstName, String newLastName)
    {
		firstName = newFirstName;
		lastName = newLastName;
	}


	//
	// The getAge method returns the employee's age
	//
    public int getAge()
    {
		return age;
	}


	//
	// The incAge method increments the employee's age
	//
    public void incAge()
    {
		// Do a sanity check
		//if (age < 65)
		if (age < retirementAge)
			age++;
	}


	//
	// The class method setRetirementAge sets the class variable
	//
	public static void setRetirementAge(int newAge)
	{
		retirementAge = newAge;
	}

}
