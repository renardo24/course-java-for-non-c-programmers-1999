public class Votes2
{
	public static void main(String[] args)
	{
		// Declare a variable capable of holding a reference to an array
		Candidate[] candidates;

		// Create an array capable of referencing 3 candidates
		candidates = new Candidate[3];

		// Initialise array with 3 candidates
		for (int i = 0; i < candidates.length; i++)
			candidates[i] = new Candidate("Candidate " + (i+1));

		// Allocate candidates with random number of votes
		for (int i = 0; i < candidates.length; i++)
			candidates[i].votes = (int)(10000 * Math.random());

		// Print contents of array
		System.out.println("\nThe results of the election were:\n");
		for (int i = 0; i < candidates.length; i++)
			System.out.println("  " + candidates[i].name
										+ "   " + candidates[i].votes + "\n");
	}

}
