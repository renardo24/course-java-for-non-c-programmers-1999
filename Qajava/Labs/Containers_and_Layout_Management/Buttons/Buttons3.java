import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;

/*
 *
 * Buttons 3
 *
 */

//
// ToDo:
//
//
// Use a border layout together with panels
// to align buttons vertically on right-hand
// side of the applet.
//

public class Buttons3 extends Applet
{
	Button b1, b2;
	TextField text;

	int count = 0;

	public void init() 
	{
		//
		// ToDo:
		//
		// Set the applet to border layout.
		//


		//
		// ToDo:
		//
		// Create two panels: one for the text field
		// and the other for the buttons.
		//



		//
		// ToDo:
		//
		// Set layout of buttons panel to grid layout
		// in order to align buttons vertically.
		// Specify 3 rows, 1 column with vertical
		// gap between components of 5 pixels
		//




		//
		// Create the text field
		//
		text = new TextField("0", 3);

		//
		// Create the buttons
		//
		b1 = new Button("Increment");
		b2 = new Button("Decrement");

        // For each Button, use an anonymous class
        // to create an implementation of ActionListener
        // which increments or decrements the count
        // and register it with the Button

	
		b1.addActionListener(new ActionListener()
		{
		    public void actionPerformed(ActionEvent evt)
		    {
                // increment the count and set the text of the Label to (""+count)
				count++;
				text.setText("" + count);
		    }
		});
  

        b2.addActionListener(new ActionListener()
		{
		    public void actionPerformed(ActionEvent evt)
		    {
                // increment the count and set the text of the Label to (""+count)
				count--;
				text.setText("" + count);
		    }
		});



		//
		// ToDo:
		//
		// Add the text field and the buttons to their
		// respective panels
		//
		//
		// Note: You will need to modify the following code
		//
		add(text);
		add(b1);
		add(b2);






		//
		// ToDo:
		//
		// Add the two panels to the applet
		//





	}


	//
	// ToDo:
	//
	// Override getInsets() method of the Container class to 
	// inset all components by 10 pixels from edges of applet container
	//


}