/* PersonTest is a Java application for testing the Person class */

public class PersonTest
{

	// Class variables to hold sample test values
	//
	private static String[] testNames = {"Hill", "Diniz", "Villeneuve",
											"Frentzen", "Schumacher", "Irvine",
											"Alesi", "Berger", "Hakkinen",
											"Panis", "Herbert", "Larini" };
	private static int[] testAges = {36, 26, 25, 29, 28, 31, 32, 37, 28, 30, 32, 32};


	//
	// The main() method is the entry point of this class
	//
	public static void main(String[] args)
	{
		//
		// ToDo
		//
		// Declare a variable capable of referencing an array of persons
		//
		


		//
		// ToDo
		//
		// Create an array capable of referencing some number of persons
		//



		//
		// ToDo
		//
		// Fill the array with references to Person objects
		// (Use test values provided in class variables above)
		//



		//
		// ToDo
		//
		// Print out initial contents of array
		//
		System.out.println("\nThe original list of people is:\n");

		

		//
		// ToDo
		//
		// Sort array so that names are either in age 
		// or alphabetical order
		//

		


		//
		// ToDo
		//
		// Print sorted contents of array
		//
		System.out.println("\nThe sorted list of people is:\n");





	}

}
