import java.io.*;

// Use InputStreamReader and OutputStreamReader classes to convert
// character stream from one encoding to another.

public class ConvertEncodingsTest
{

	public static void convert(String infile, String outfile,
									String from, String to)
							throws IOException, UnsupportedEncodingException
	{
		// set up byte streams
		InputStream in;
		if (infile != null)
			in = new FileInputStream(infile);
		else
			in = System.in;
		
		OutputStream out;
		if (outfile != null)
			out = new FileOutputStream(outfile);
		else
			out = System.out;

		// use default encoding of not specified
		if (from == null)
			from = System.getProperty("file.encoding");
		if (to == null)
			to = System.getProperty("file.encoding");

		// set up character streams
		Reader r = new BufferedReader(new InputStreamReader(in, from));
		Writer w = new BufferedWriter(new OutputStreamWriter(out, to));

		// copy characters from input to output.
		// InputStreamReader converts input encodingto Unicode.
		// OutputStreamReader converts Unicode to output encoding.
		char[] buffer = new char[4096];
		int len;

		while ((len = r.read(buffer)) != -1)
			w.write(buffer, 0, len);

		r.close();
		w.flush();
		w.close();
	}


	public static void main(String[] args) throws Exception
	{
		if (args.length == 0)
			convert(null, null, null, null);
		if (args.length == 2)
			convert(args[0], args[1], null, null);
		else if (args.length == 4)
			convert(args[0], args[1], args[2], args[3]);
		else
			System.out.println("Usage: java ConvertEncodingsTest " +
								 "<in_file> <out_file> " +
								 "<in_encoding> <out_encoding>" +
								 "e.g.java ConvertEncodingsTest " +
								 "infile.text outfile.txt ISO-8859-1 Cp863");
	}

}