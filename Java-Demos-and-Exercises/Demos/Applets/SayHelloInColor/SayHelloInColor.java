import java.applet.*;
import java.awt.*;

public class SayHelloInColor extends Applet
{

	public void init()
	{
		setBackground(Color.blue);
	}


	public void paint(Graphics g)
	{
		g.setColor(Color.cyan);
		g.drawString("Hello again!", 5, 50);
	}

}
