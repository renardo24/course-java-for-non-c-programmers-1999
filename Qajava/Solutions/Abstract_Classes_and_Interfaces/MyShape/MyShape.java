import java.awt.*;

/*
 *
 * MyShape
 *
 */

public abstract class MyShape 
{
	// Instance variables for left, top, width and height
	// Note use of protected modifier to allow direct access by a subclass
	//
	protected int left, top, width, height;

	//
	// Constructor
	//
	public MyShape(int l, int t, int w, int h)
	{
		left = l;
		top = t; 
		width = w; 
		height = h;
	}


	//
	// Force all our subclasses to implement a draw() method
	// to draw themselves on the specified Graphics object
	//
	public abstract void draw(Graphics g);

}
