import java.applet.*;
import java.awt.*;
import java.util.*;

// This version uses offscreen image to eliminate flashing.  This 
// technique is known as double buffering.

public class DigitalClock3 extends Applet implements Runnable
{
	Thread thread;
	Image bImage;
	Graphics bg;

	public void init()
	{
		// create offscreen image, same size as applet
		bImage = createImage(size().width, size().height);
		bg = bImage.getGraphics();
	}

	
	public void start()
	{
		if (thread == null)
		{
			thread = new Thread(this);
			thread.start();
		}
	}

	public void run()
	{
		while (true)
		{
			repaint();
			try
			{
				Thread.sleep(100);
			} catch (InterruptedException e)
			{}
		}
	}

	// override update() method; do NOT clear background

	public void update(Graphics g)
	{
		paint(g);
	}

	public void paint(Graphics g)
	{
		// first, build offscreen image
		Font f = new Font("Helvetica", Font.BOLD, 36);
		bg.setFont(f);
		Date now = new Date();
		bg.setColor(getBackground());
		bg.fillRect(0, 0, size().width, size().height);
		bg.setColor(Color.blue);
		bg.drawString(now.toLocaleString(), 0, 50);

		// now, transfer offscreen image to display
		g.drawImage(bImage, 0, 0, this);
	}

	public void stop()
	{
		thread.stop();
		thread = null;
	}

	public void destroy()
	{
		bg.dispose();
	}

}
