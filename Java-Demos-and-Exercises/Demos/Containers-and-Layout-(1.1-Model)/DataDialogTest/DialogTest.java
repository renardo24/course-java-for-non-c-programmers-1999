import java.awt.*;
import java.awt.event.*;

public class DialogTest extends Frame
{
	TextField feedback;

	public DialogTest()
	{
		Button login;

		add(login = new Button("Login"));
		add(feedback = new TextField(30));
		feedback.setEditable(false);
		setLayout(new FlowLayout());

		login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt)
			{
				feedback.setText("");
				LoginDialog dialog = new LoginDialog(DialogTest.this);
				dialog.show();  // blocks until user dismisses dialog

				if (dialog.getPassword().equalsIgnoreCase("Java"))
					feedback.setText("Welcome to Java, " + dialog.getName());
				else
					feedback.setText("Sorry, please try again.");

				dialog = null;  // finished with dialog
			}
		});

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we)
			{
				System.exit(0);
			}
		});

	}


	public static void main(String[] args)
	{
		Frame f = new DialogTest();
		f.setSize(300, 200);
		f.show();
	}


}

