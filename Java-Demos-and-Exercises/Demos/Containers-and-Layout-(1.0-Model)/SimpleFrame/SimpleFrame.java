import java.awt.*;

// display an empty frame window

public class SimpleFrame extends Frame
{

	public SimpleFrame(String s)
	{
		super(s);
		resize(300, 200);
		show();
	}

	// must handle window-destroy events to
	// allow user to quit application
	public boolean handleEvent(Event evt)
	{
		if (evt.id == Event.WINDOW_DESTROY)
			System.exit(0);
		return super.handleEvent(evt);
	}


	public static void main(String[] args)
	{
		Frame f = new SimpleFrame("A Simple Frame Window");
	}

}
