import java.awt.*;
import java.awt.event.*;
import java.io.*;

/*
 *
 * HandleException
 *
 */
public class HandleException extends Frame
{
	// The object "text" contains the string
	// the user types
	TextField text = null;

	// The object "box" is where to display 
	// your messages to the user
	TextArea box = null;

	class OpenFileBehaviour implements ActionListener
	{
	    public void actionPerformed(ActionEvent event)
	    {
			// 
			// ToDo: 
			//
			// Use the commands below this comment
			// block to open and close the file.
			//
			// You will need to	surround this 
			// with exception handling. Display
			// a message to tell the user if the
			// file was opened successfully or
			// not and which exception was 
			// generated.
			//
			// Use the following method to display
			// your messages to the user:
			//
			//	box.setText("my message");
			//
			FileInputStream in = 
				new FileInputStream(text.getText());
			in.close();
			
	    }
	}
	

	//
	// The constructor sets up the interface for you.
	// There is nothing you need to add to this.
	//
	public HandleException()
	{
		setTitle("HandleException");
		setLayout(new BorderLayout());
		
		// 
		// Set up panel to hold label, 
		// text field and button
		//
		Panel p = new Panel();
  		p.add(new Label("Enter absolute path name: "));
		
		//
		// Text field to hold file name. Keep a
		// reference to it since we need to
		// recover the contents when the user
		// presses the "Open File" button.
		//
		text = new TextField(30);

		p.add(text);

        Button openFileButton = new Button("Open File");
		p.add(openFileButton);
		openFileButton.addActionListener(new OpenFileBehaviour());

		add("South", p);
		
		//
		// Feedback box. Again, keep a reference
		// so that we can write messages into it
		// when trying to open files.
		//
		box = new TextArea(3, 50);
		box.setText("Enter a file name below");

		add("North", box);

		addWindowListener(new MyWindowBehaviour());

}

 
	//
	// This handles end of window.
	// There is nothing you need to add to this.
	//
	class MyWindowBehaviour extends WindowAdapter
	{
	    public void windowClosing(WindowEvent event)
	    {
		//
		// If the user closes our main window
		// then exit.
		//
	        dispose();
	        System.exit(0);
	    }
	}
	
	
	//
	// This sets up the window.
	// There is nothing you need to add to this.
	//
	public static void main(String[] args)
	{
		//
		// Create frame-based object.
		//
		Frame f = new HandleException();
		
		//
		// Set sensible size and display.
		//
		f.resize(500, 200);
		f.show();
	}

}