/*
 * EmployeeTest class
 *
 * EmployeeTest is a Java application for testing the Employee class.
 *
 */

public class EmployeeTest
{
	//
	// The main() method is the entry point of this class
	//
	public static void main(String[] args)
	{
		//
		// Declare two references to Employee objects
		//
		Employee employee1, employee2;
		
		//
		// Create two new Employee objects
		//
	    employee1 = new Employee("Smith", 28);
        employee2 = new Employee("Jones", 25);

		// Display headings for employee's details
		//
		System.out.println("\n" + "Employee" + "\t" + "Age" + "\n");

		// Display details of both employees
		//

		System.out.println(employee1.name + "\t\t" + employee1.age);
		System.out.println(employee2.name + "\t\t" + employee2.age);

	}

}
