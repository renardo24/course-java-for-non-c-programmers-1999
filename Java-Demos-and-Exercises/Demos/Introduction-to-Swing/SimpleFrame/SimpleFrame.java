import com.sun.java.swing.*;

// display an empty frame window
// updated to use JFC

public class SimpleFrame extends JFrame
{
	public SimpleFrame(String title)
	{
	    super(title);
		setSize(300, 200);
		setVisible(true);
	}

	public static void main(String[] args)
	{
		JFrame f = new SimpleFrame("Simple Frame");
	}

}
