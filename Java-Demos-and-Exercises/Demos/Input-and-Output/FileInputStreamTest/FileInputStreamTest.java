import java.io.*;

public class FileInputStreamTest
{

	public static void main(String[] args) throws IOException
	{
		InputStream in;

		if (args.length == 1)
			in = new FileInputStream(args[0]);
		else
			in = System.in;

		int ch;
		int nChars = 0;
		int nSpaces = 0;

		for (; (ch = in.read()) != -1; nChars++)
		{
			if (Character.isSpace((char)ch))
				nSpaces++;
		}
		System.out.println("Counted " + nChars + " characters, including "
								+ nSpaces + " white spaces");
	}

}

