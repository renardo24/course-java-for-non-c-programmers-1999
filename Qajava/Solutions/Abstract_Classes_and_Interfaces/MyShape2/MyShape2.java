import java.awt.*;

/*
 *
 * MyShape
 *
 */

public abstract class MyShape2 
{
	// Instance variables for left, top, width and height
	// Note use of protected modifier to allow direct access by a subclass
	//
	protected int left, top, width, height;

	//
	// Constructor
	//
	public MyShape2(int l, int t, int w, int h)
	{
		left = l;
		top = t; 
		width = w; 
		height = h;
	}


	//
	// No methods
	//

}
