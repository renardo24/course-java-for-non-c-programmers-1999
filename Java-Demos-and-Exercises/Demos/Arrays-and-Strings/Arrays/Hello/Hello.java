public class Hello
{
	public static void main(String[] args)
	{
		if (args.length != 1)
			System.out.println("\nUsage: Hello <name>");
		else
			System.out.println("\nHello " + args[0] + "!");
	}

}