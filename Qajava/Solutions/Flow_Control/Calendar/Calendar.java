/* Using the switch statement */

public class Calendar
{
	public static void main(String argv[])
	{
        byte currentMonth = 3;     // a number between 0 and 11
		boolean leapYear = false;

		// First, display current month

		// Note: we could use another switch statement here to
		// convert month number to a string (or better still, use
		// an array of pre-initialised strings)

		System.out.println("The current month is " + (currentMonth+1));


		// Declare a variable for result

		byte daysInMonth;

		// Use switch statement to work out days in the current month

		switch (currentMonth)
		{
			case 0:
			case 2:
			case 4:
			case 6:
			case 7:
			case 9:
			case 11:
				daysInMonth = 31;
				break;

			case 8:
			case 3:
			case 5:
			case 10:
				daysInMonth = 30;
				break;

			case 1:
				// First attempt
				if (leapYear)
					daysInMonth = 29;
				else
					daysInMonth = 28;
				// Second attempt
				//daysInMonth = leapYear ? 29 : 28;
				break;

			default:
				// Illegal month - set error value
				daysInMonth = 0;
				break;

		}

		// Check that we got a result
		// If so, display result, otherwise display error message

		if (daysInMonth != 0)
			System.out.println("There are " + daysInMonth
									+ " days in the current month");
		else
			System.out.println("Illegal month specified");
		
    }

}

