import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;

/*
 *
 * Components
 *
 */

public class Components extends Applet
                        implements ActionListener
{
	TextArea	feedback;
	TextField	name;
	Choice		relation;
	Button      ok, reset;

	public void init()
	{
		//
		// ToDo:
		//
		// Create a non-editable text area of
		// 5 lines by 40 columns. This is used
		// to display messages to the user.
		//
		// Initialise it with an appropriate
		// message and assign the new TextArea to
		// the instance variable named feedback.
		//

      	//
		// ToDo:
		//
		// Create a Label to display the text "Name", 
		// and a TextField for the user to
		// type a name into. Assign the TextField
		// to the instance variable called name.




		//
		// ToDo:
		//
		// Create a Label to display the text "Relationship".
		//
		
		//
		// ToDo:
		//
		// Assign a new Choice object to the 
		// relation instance variable. Add the possible
		// relationship types to the Choice. Examples would be:
		//
		//	Father, Mother, Sister, Brother,
		// 	Son, Daughter, Niece, Nephew
		//




		//
		// ToDo:
		//
		// Assign two new Buttons to the instance variables
		// ok and reset. Label the Buttons appropriately.
		//


		// 
		// ToDo:
		//
		// Add all the components to the applet.
		//
		
		//
		// ToDo:
		//
		// Register the applet as an ActionListener
		// for the two Buttons ok and reset.
		//

}	

    public void actionPerformed(ActionEvent evt){

		//
		// ToDo:
		//
		// Discover if the Event Source was ok or reset.
		//
		// If the source was the ok Button, check that
		// the user typed a name in the TextField.
		// If so say "Hi" to the named relation
		// by displaying a message in the TextArea
		// (e.g. "Hello there Uncle Albert").
		// Otherwise, display an error message.
		// 	
		
		//
		// ToDo:
        //
		// If the event source was the reset Button,
		// clear the TextField and reset the Choice 
		// relationship to its original value.
		// Show a message prompting the user to start again.
        //
        
	}


}


