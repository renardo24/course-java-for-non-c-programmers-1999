import java.awt.*;
import java.awt.event.*;


// display an empty frame window
// updated to use Java 1.1 event model

public class AboutDialogTest extends Frame
{
	Button aboutButton;
	AboutDialog dialog;

	public AboutDialogTest()
	{
		setLayout(new FlowLayout());

		add(aboutButton = new Button("About"));

		aboutButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt)
			{
				dialog = new AboutDialog(AboutDialogTest.this, "About ...");
				dialog.show();
			}
		});

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt)
			{
				System.exit(0);
			}
		});

		setSize(300, 200);        // resize() has been deprecated
		show();

	}


	public static void main(String[] args)
	{
		Frame f = new AboutDialogTest();
	}

}
