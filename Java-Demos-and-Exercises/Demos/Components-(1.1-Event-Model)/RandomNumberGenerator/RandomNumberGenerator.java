import java.applet.*;
import java.awt.*;
import java.awt.event.*;

public class RandomNumberGenerator extends Applet implements ActionListener
{
	private Button generate;
	private Label random;

	public void init()
	{
		add(new Label("Random Number Generator"));
		add(generate = new Button("Generate"));
		add(random = new Label("         "));
		
		generate.addActionListener(this);
	}


	public void actionPerformed(ActionEvent evt)
	{
		if (evt.getSource() == generate)
		{
			int randomNum = (int)(10000 * Math.random());
			random.setText("" + randomNum);
		}
	}
 
}