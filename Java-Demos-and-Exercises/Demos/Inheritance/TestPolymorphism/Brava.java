public class Brava
{
	private int numDoors;
	protected Engine engine;
	private int gear; 

	public Brava(int d, Engine e)
	{
		numDoors = d;
		engine = e;
	}


	public boolean start()
	{
		return engine.start();
	}


	public void selectGear(int g)
	{
		gear = g;
	}


	public int getSpeed()
	{
		return (engine.getRevs()/100) * gear;
	}


	public String toString()
	{
	  return getClass().getName();
	}

}
