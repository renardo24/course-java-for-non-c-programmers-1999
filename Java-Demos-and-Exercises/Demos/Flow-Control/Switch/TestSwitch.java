public class TestSwitch
{
	public static void main(String[] args)
	{
		int choice = 0;

		if (args.length > 0)
			choice = Integer.parseInt(args[0]);
		
		switch (choice)
		{
			case 37:
				System.out.print("Coffee? ");
				break;

			case 45:
				System.out.print("Tea? ");
				break;

			default:
				System.out.println("Please try again");
				break;

		}
		System.out.println();
	}

}