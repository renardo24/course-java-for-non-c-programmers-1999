import java.applet.*;
import java.awt.*;

public class SayHelloWithParameters extends Applet
{
	String fontName = "TimesRoman";
	int fontStyle = Font.PLAIN;
	int pointSize = 12;
	Font font;
	String text = "";	

	public void init()
	{
		String s = getParameter("font");
		if (s.equalsIgnoreCase("Courier"))
			fontName = "Courier";
		else if (s.equalsIgnoreCase("Helvetica"))
			fontName = "Helvetica";
		else if (s.equalsIgnoreCase("Times Roman"))
			fontName = "TimesRoman";

		s = getParameter("style");
		if (s.equalsIgnoreCase("Plain"))
			fontStyle = Font.PLAIN;
		else if (s.equalsIgnoreCase("Bold"))
			fontStyle = Font.BOLD;
		else if (s.equalsIgnoreCase("Italic"))
			fontStyle = Font.ITALIC;

		s = getParameter("size");
		if (s != null)
			pointSize = Integer.parseInt(s);

		s = getParameter("text");
		if (s != null)
			text = s;
	}


	public void paint(Graphics g)
	{
		font = new Font(fontName, fontStyle, pointSize);
		g.setFont(font);
		g.drawString(text, 5, 50);
	}

}
