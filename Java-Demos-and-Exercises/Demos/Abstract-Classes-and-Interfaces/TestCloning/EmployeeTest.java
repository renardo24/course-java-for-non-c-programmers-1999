public class EmployeeTest
{

	public static void main(String[] args)
	{
		Employee e1, e2, e3;
		
		e1 = new Employee("Smith");
		e2 = new Employee("Jones");

		System.out.println("Employee e1 is " + e1);
		System.out.println("Employee e2 is " + e2);

		try
		{
			e3 = (Employee)e1.clone();
			System.out.println("Employee e3 is " + e3);		

			e1.insertFirstName("David");
			e3.insertFirstName("John");

			System.out.println();
			System.out.println("Employee e1 is " + e1);
			System.out.println("Employee e2 is " + e2);
			System.out.println("Employee e3 is " + e3);		
		}
		catch (CloneNotSupportedException e)
		{
			e.printStackTrace();
		}

	}

}
