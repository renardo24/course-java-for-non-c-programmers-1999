import java.applet.*;
import java.awt.*;

// Test multithreaded output to two TextArea components

public class ThreadedOutputTest1 extends Applet
{
	private TextArea txt1, txt2;
	private ThreadedOutput msg1, msg2, msg3;


	public void init()
	{
		txt1 = new TextArea(10, 40); // 10 rows, 40 cols
		txt2 = new TextArea(10, 40); // 10 rows, 40 cols
		add(txt1);
		add(txt2);

		// first two threads output to first text area
		msg1 = new ThreadedOutput("This is the first thread\n", txt1);
		msg2 = new ThreadedOutput("This is the second thread\n", txt1);

		// third thread outputs to second text area
		msg3 = new ThreadedOutput("This is the third thread\n", txt2);
	}


	public void start()
	{
		// create and start three threads
		Thread thread1 = new Thread(msg1);
		thread1.start();

		Thread thread2 = new Thread(msg2);
		thread2.start();

		Thread thread3 = new Thread(msg3);
		thread3.start();
	}


}
