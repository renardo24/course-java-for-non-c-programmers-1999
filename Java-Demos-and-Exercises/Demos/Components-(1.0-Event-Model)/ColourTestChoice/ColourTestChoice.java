import java.applet.*;
import java.awt.*;

// Using a Choice component

public class ColourTestChoice extends Applet
{
	private Choice colourChoice;

	public void init()
	{
		add(new Label("Background colour:"));
		add(colourChoice = new Choice());
		colourChoice.addItem("Red");
		colourChoice.addItem("Green");
		colourChoice.addItem("Blue");
		colourChoice.addItem("White");
		colourChoice.addItem("Black");

		setBackground(Color.white);
		colourChoice.select("White");  // select white as initial colour
	}


	public boolean action(Event evt, Object arg)
	{
		boolean result = false;
		if (evt.target == colourChoice)
		{
			changeColor((String)arg);
			result = true;
		}
		return result;
	}

	private void changeColor(String s)
	{
		if (s.equals("Red")) setBackground(Color.red);
		else if (s.equals("Green")) setBackground(Color.green);
		else if (s.equals("Blue")) setBackground(Color.blue);
		else if (s.equals("White")) setBackground(Color.white);
		else if (s.equals("Black")) setBackground(Color.black);
		repaint();
	}

}
