/*
 *
 * Student
 *
 * A student has a name and age inherited from
 * Person and a subject all of its own.
 *
 */

public class Student extends Person
{
	//
	// ToDo
	//
	// Declare an additional instance variable for the student's subject



	//
	// Todo
	// Provide a constructor to initialise instance variables
	// Do not forget to construct superclass first
	//




	//
	// Todo
	//
	// Override getDetails() instance method of superclass to return
	// full details of student (age and subject) in a string, e.g.,
	// "32 + ", " + "Physics" (but, of course, not hardcoded like this!)
	//
	// Note that this method should not attempt to access
	// instance variables of superclass directly, because
	// (a) it is bad practice
	// (b) variables may be private to superclass
	// 

	


	//
	// Todo
	//
	// Provide an instance method to change student's subject
	//





}