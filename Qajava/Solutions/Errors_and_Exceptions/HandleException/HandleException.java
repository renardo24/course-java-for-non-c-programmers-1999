import java.awt.*;
import java.io.*;
import java.awt.event.*;

/*
 *
 * HandleException
 *
 */
public class HandleException extends Frame
{
	TextField text = null;
	TextArea box = null;

	public HandleException()
	{
		setTitle("HandleException");
		setLayout(new BorderLayout());
		
		// 
		// Set up panel to hold label, 
		// text field and button
		//
		Panel p = new Panel();
  		p.add(new Label("Enter absolute path name: "));
		
		//
		// Text field to hold file name. Keep a
		// reference to it since we need to
		// recover the contents when the user
		// presses the "Open File" button.
		//
		text = new TextField(30);

		p.add(text);
        
        Button openFileButton = new Button("Open File");
		p.add(openFileButton);
		openFileButton.addActionListener(new OpenFileBehaviour());

		add("South", p);

		//
		// Feedback box. Again, keep a reference
		// so that we can write messages into it
		// when trying to open files.
		//
		box = new TextArea(3, 50);
		box.setText("Enter a file name below");

		add("North", box);
		
		addWindowListener(new MyWindowBehaviour());

}
	
	class MyWindowBehaviour extends WindowAdapter
	{
	    public void windowClosing(WindowEvent event)
	    {
	        dispose();
	        System.exit(0);
	    }
	}
	
	class OpenFileBehaviour implements ActionListener
	{
	    public void actionPerformed(ActionEvent event)
	    {
			//
			// Try opening and closing the
			// file given by the user. Give
			// the user a message indicating
			// success or failure in the
			// feedback box.
			//
			try
			{
				FileInputStream in = 
					new FileInputStream(text.getText());
				in.close();
				box.setText("Opened file " + 
							text.getText() + 
							" successfully");
			}
			catch (FileNotFoundException e)
			{
				box.setText("Can't open file: " + e);
				System.out.println("Could not open file: " );
			}
			catch (IOException e)
			{
				box.setText("Problem closing file: " + e);
				System.out.println("Problem closing file: " + e);
			}
	    }
	}
	

	public static void main(String[] args)
	{
		//
		// Create frame-based object.
		//
		Frame f = new HandleException();
		
		//
		// Set sensible size and display.
		//
		f.resize(500, 200);
		f.show();
	}

}