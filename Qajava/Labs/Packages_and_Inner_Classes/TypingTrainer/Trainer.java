import java.awt.*;
import java.awt.event.*;
import java.applet.*;
import java.util.*;
import java.text.*;

public class Trainer extends Applet
{

protected final int width = 60;           // width of text displays

// components
protected Button start = new Button("Start");
protected Button stop = new Button ("Stop");
protected TextField userText = new TextField("",width);     // box where user types in text, width columns wide
protected TextArea displayTest = new TextArea("",1,width,TextArea.SCROLLBARS_NONE); // box where text-to-be-typed is displayed
protected Label errorLabel = new Label("Press Start When Ready");    // label to show number of mistakes made
protected Label message = new Label("Type your text here   : ");     // label to display messages

protected TypingTutor typingtutor;                                  // an instance of a member class TypingTutor

    public void init()
    {
        // Add Components
        setLayout(new FlowLayout());
        add(new Label("Text For You To Type"));
        add(displayTest);
        add(message);
        add(userText);
        add(errorLabel);
        add(start);
        add(stop);

        // Set displayText to be non-editable
        displayTest.setEditable(false);

        //
        // ToDo:
        //
        // disable the stop Button using the setEnabled()
        // method from the Component class.
        //



        //
        // ToDo:
        //
        // Add a non-editable TextArea, 5 rows deep and width+1 columns wide,
        // and having a vertical scrollbar.
        //


        //
        // ToDo:
        //
        // Use an anonymous inner class to provide an ActionListener
        // for the "userText" TextField. When the user presses enter (and thus creates
        // and ActionEvent), copy the contents of the "userText" TextField to the
        // "log" TextArea, and then clear the "userText" TextField.
        //



        //
        // ToDo:
        //
        // create a new instance of the member class TypingTutor and
        // assign it to the Applet's instance field "typingtutor"
        //


        //
        // ToDo:
        //
        // Register the start Button with an anonymous ActionListener.
        // When the start Button is pressed, the ActionListener
        // should disable the start Button and enable the stop Button.
        // Then the method initialised() should be called on the variable typingtutor
        //


        //
        // ToDo:
        //
        // Register the stop Button with
        // an anonymous ActionListener. When stop is pressed,
        // the ActionListener should  call the
        // typingtutor's reset() method.
        //


} // ends init()



    // Definition of a member class called TypingTutor which extends KeyAdapter
   private class TypingTutor extends KeyAdapter
    {

        //
        // ToDo:
        //
        // Provide an initialise() method for the TypingTrainer:
        //
        // In the initialise() method:
        // Initialise the TextArea "displayTest" to show as many
        // characters from the TypingTutor's String instance variable "str" as
        // are indicated by its "preview" variable.
        // [Use the substring() method of the String class to do this.]
        //
        // Initialise "errorLabel" to show the text " Errors: 0 ", and
        // clear the TextField userText. The userText TextField could also use
        // the requestFocus() method of the Component class to gain the focus.
        //
        // Register the TypingTrainer with the Applet's TextField "userText"
        //


        public void reset()
        {

        // ToDo:
        //
        // De-register the TypingTrainer as a listener for
        // the Applet's userText TextField.

        // ToDo:
        //
        // disable the stop button, enable the start button.

        // ToDo:
        //
        // Empty the TextArea "displayTest"
        //


         // Initialise the TypingTrainer's instance variables:
        initialiseVariables();
        }


        public void keyTyped(KeyEvent te)
           {
                // ToDo:
                //
                // get the character key which caused the KeyEvent
                // [use the getKeyChar() method from the KeyEvent class]


                // ToDo:
                //
                // if the character is identical to the character at the
                // "current" position in the String "str", then call
                // the TypingTutor's processNextKey() method
                //
                // If the two characters do not match call the TypingTutor's mistake() method.
                //


           }        // ends keyTyped()



/*  *** It is not intended that you should alter the method beyond this point *** */


         public void initialiseVariables()
         {
            current = 0;
            error = 0;
            linePosition = 0;
            show = preview;
            mark = width;
            message.setText(" ");
         }


         public void processNextKey()                      // display next key (if appropriate)
            {

                if (++current >= str.length())             // if no more characters to type,
                {
                           reset();                        // call reset()
                           message.setText("Thankyou - but that's all!");
                           return;
                }
                else if(current == mark)                   // if end of current line
                           newLine();                      // display text on new line

                else {
                        linePosition ++;

                        if(show < str.length()  )           // if more chars to show
                        {
                           // set mark if in margin area and the previous character to be shown was a whitespace character.
                           if((linePosition > width-margin)  && Character.isWhitespace(str.charAt(show-1)) && (linePosition <width)){
                                mark = show;
                           }
                           // reveal next character and increment show
                           displayTest.append(str.substring(show,++show));
                        }
                     }
                message.setText("  ");

             }



           public void newLine()                        // begin new line of characters in displayTest
           {
                linePosition = 0;
                show = mark + preview;
                if(show < str.length())                     // if sufficient chars remain
                    displayTest.setText(str.substring(mark, show)); // display first group of chars
                else displayTest.setText(str.substring(mark)); // else display the last chars

                mark += width;
           }


           public void mistake(char c)         // key user typed does not match the current character
           {
                if(! Character.isISOControl(c)){       // unless key is an ISO control e.g. backspace
                    sound.play();                               // sound error message
                    errorLabel.setText("Errors: " + ++error);   // display new error total
                    if(str.charAt(current) == '\u0020'){        // display message to user
                        message.setText(" Hit the SPACE bar!");
                    }
                    else{
                        message.setText("Type: " + str.charAt(current));
                    }
                }
           }



    // Instance fields for the member class TypingTutor :

    private int preview = 5;                     // the number of advance characters to reveal
    private int current = 0;                     // the index of the next character to be typed
    private int show = preview;                  // the last character displayed to the user
    private int error = 0;                       // the number of errors made by the user
    private int mark = width;                    // a convenient line-break point
    private int linePosition = 0;                // position on the current line
    private final int margin = 10;               // size of the line-break area
    private AudioClip sound = getAudioClip(getCodeBase(),"drip.au"); // AudioClip for audio alerts

    private String str = "Who else is there of the direct blood-line? Only the vacant Aunts, Cora and Clarice, " +
                         "the identical twins and sisters of Sepulchrave. So limp of brain that for them to " +
                         "conceive an idea is to risk a haemorrhage. So limp of body that their purple dresses " +
                         "appear no more indicative of housing nerves and sinews than when they hang suspended "+
                         "from their hooks. Of the others? The lesser breed? In order of social precedence, " +
                         "possibly the Prunesquallors first, that is, the Doctor and his closely-swathed and " +
                         "bone-protruding sister. The doctor with his hyena laugh, his bizarre and elegant body, " +
                         "his celluloid face.";

    } // ends member class TypingTutor


}     // ends Applet
