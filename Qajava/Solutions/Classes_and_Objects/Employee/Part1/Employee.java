/* Class for employee */
 
public class Employee
{
	// Variables to hold employee's name and age
	public String name;
    public int age;

	//
	// Constructor that takes a String and an int argument
	// to initialise the instance variables for the employee's name
	// and age, respectively.
	//
    public Employee(String n, int a)
    {
		name = n;
		age  = a;
	}


}
