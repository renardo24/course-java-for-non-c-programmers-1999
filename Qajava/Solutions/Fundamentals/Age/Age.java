/* Using variables, assignments and operators */

public class Age
{
	public static void main(String argv[])
	{
		//
		// ToDo:
		//
		// Add your code here
		//

		// First attempt: declare variables individually

		//short myAge;		// use mixed case
		//short yourAge;	// must not start with a digit

		//myAge = 35;
		//yourAge = 32;

		// Second attempt: use comma separated list of variables
		
		//short myAge, yourAge;	
		//myAge = 35;
		//yourAge = 32;

		// Third attempt: declare and initialise both variables together

		short myAge = 36, yourAge = 32;

		short ageDifference, averageAge;

		char mySex = 'M', yourSex = 'F';

		// First attempt: just print out ages
		//System.out.println(myAge);
		//System.out.println(yourAge);

		// Second attempt: print out meaningful string (use + operator)
		System.out.println("My age is " + myAge);
		System.out.println("Your age is " + yourAge);


		// Need to cast result to a short, as result will be an int
		// Note: wouldn't need to cast if variables were ints

		ageDifference = (short) (myAge - yourAge);
		System.out.println("We differ in age by " + ageDifference + " years");

		averageAge = (short)((myAge + yourAge)/2);
		System.out.println("Our average age is " + averageAge);

		// First attempt: legal and works but difficult to read
		//boolean bothOver30 = myAge >= 30 && yourAge >= 30;


		// Second attempt: no difference, but is easier to read
		boolean bothOver30 = (myAge >= 30) && (yourAge >= 30);
		System.out.println("Both over 30:  " + bothOver30);

		boolean oneUnder25 = (myAge < 25) || (yourAge < 25);
		System.out.println("One under 25:  " + oneUnder25);

		boolean oneMaleOver40 = (myAge >= 40) && (mySex == 'M' || mySex == 'm')
									|| (yourAge>= 40) && (yourSex == 'M' || yourSex == 'm');
		System.out.println("One male over 40: " + oneMaleOver40);

		System.out.println("End of program");
	}
}

