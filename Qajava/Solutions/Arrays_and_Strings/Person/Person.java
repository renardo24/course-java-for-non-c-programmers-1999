/* Person is a class to represent a simple person */

public class Person
{

	// Declare instance variables for person's name and age
	private String name;
	private int age;
	
	// Constructor to initialise person's name and age
	//
	public Person(String n, int a)
	{
		name = n;
		age = a;
	}

	// Method to get person's name
	//
	public String getName()
	{
		return name;
	}

	// Method to get person's age
	//
	public int getAge()
	{
		return age;
	}


	// Provide a compareToPerson() method similar to String.compareTo()
	//
	public int compareTo(Person other)
	{
		return name.compareTo(other.name);
	}


	// Provide a class method to sort persons in array by age
	// (use simple bubble sort algorithm)
	//
	public static void bSortByAge(Person[] pers)
	{
		for (int i = 0; i < pers.length; i++)
		{
			for (int j = i + 1; j < pers.length; j++)
			{
				if (pers[i].age > pers[j].age)
				{
					// swap order
					Person temp = pers[j];
					pers[j] = pers[i];
					pers[i] = temp;
				}
			}
		}
	}


	// Provide a class method to sort persons in array by name
	// (use simple bubble sort algorithm)
	//
	public static void bSortByName(Person[] pers)
	{
		for (int i = 0; i < pers.length; i++)
		{
			for (int j = i + 1; j < pers.length; j++)
			{
				if (pers[i].compareTo(pers[j]) > 0)
				{
					// swap order
					Person temp = pers[j];
					pers[j] = pers[i];
					pers[i] = temp;
				}
			}
		}

	}



}
