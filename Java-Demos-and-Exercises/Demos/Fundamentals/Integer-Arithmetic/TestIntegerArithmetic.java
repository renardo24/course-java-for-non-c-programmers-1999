public class TestIntegerArithmetic
{
	public static void main(String[] args)
	{
		byte b1 = 2, b2 = 3, b3 = 0;
		b3 = b1 + b2;         // invalid
		b3 = 2 + 3;           // invalid, but works with VJ++!
		b3 = (byte)(b1 + b2); // OK with a cast
		b3 = (byte)(2 + 3);   // OK with a cast

		System.out.println("b1 = " + b1 + ", b2 = " + b2 + ", b3 = " + b3);
	}

}
