import java.applet.*;
import java.awt.*;

// using checkbox group (i.e. radio buttons)

public class ColourTestRadioButtons extends Applet
{
	Checkbox red, green, blue, white, black;

	public void init()
	{
		setBackground(Color.white);

		CheckboxGroup cbg = new CheckboxGroup();
		add(red = new Checkbox("Red", cbg, false));
		add(green = new Checkbox("Green", cbg, false));
		add(blue = new Checkbox("Blue", cbg, false));
		add(white = new Checkbox("White", cbg, true));  // initially checked
		add(black = new Checkbox("Black", cbg, false));
	}


	public boolean action(Event evt, Object arg)
	{
		if (evt.target == red) setBackground(Color.red);
		else if (evt.target == green) setBackground(Color.green);
		else if (evt.target == blue) setBackground(Color.blue);
		else if (evt.target == white) setBackground(Color.white);
		else if (evt.target == black) setBackground(Color.black);
		else return false;
		repaint();
		return true;
	}

}
