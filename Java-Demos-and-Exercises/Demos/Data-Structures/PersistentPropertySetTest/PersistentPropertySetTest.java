import java.util.*;
import java.io.*;

public class PersistentPropertySetTest
{

	public static void main(String[] args) throws IOException
	{
		// create an empty property set
		Properties prices = new Properties();

		// if it exists, load properties from disk file
		File file = new File("prices.dat");
		if (file.exists())
		{
			InputStream in = new FileInputStream(file);
			prices.load(in);
		}

		// add an item to the list
		prices.put("Tea", "1.10");

		// save changes
		OutputStream out = new FileOutputStream(file);
		prices.save(out, "Price List");

		System.out.println();

		// enumerate keys and prices held in property set
		Enumeration e = prices.propertyNames();

		while (e.hasMoreElements())
		{
			// get next key
			String key = (String)e.nextElement();  // note cast
			// now get its price
			String price = prices.getProperty(key);
			System.out.println(key + "\t" + price);
		}


	}

}