import java.io.*;

public class RandomAccessTest
{

	public static void main(String[] args)
	{
		int n = 0;  // index number

		if (args.length == 1)
			n = Integer.parseInt(args[0]);

		try
		{
			// open file for reading
			RandomAccessFile f = new RandomAccessFile("myfile.dat", "r");

			// seek to required location in file
			f.seek(n * 4);  // 4 bytes per int

			int value = f.readInt();
			System.out.println("Read " + value + " from int " + n);
			System.out.println("Next int to read is at " + (f.getFilePointer() / 4));
		}
		catch (IOException e)
		{
			System.out.println("Exception: " + e);
		}
	}

}
