public class Car
{
	private String model;

	public Car()
	{
		this("Ford");
	}

	public Car(String make)
	{
		model = make;
	}


	public String getModel()
	{
		return model;
	}


}
