public class TestCar
{
	public static void main(String[] args)
	{
		Car car1 = new Car();

		car1.selectGear((byte)1);

		for (int i = 0; i < 12; i++)
			car1.accelerate();

		System.out.println("\ncar1 is now doing " + car1.getSpeed());
	
		if (car1.getSpeed() >= 55)
		{
			if (car1.selectGear((byte)5))
				System.out.println("\ncar1 is now in 5th gear");
		}
		 
	}

}

		