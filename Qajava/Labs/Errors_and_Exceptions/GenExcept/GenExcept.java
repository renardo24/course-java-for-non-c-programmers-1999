import java.awt.*;
import java.applet.*;
import java.io.*;
import java.net.*;
import java.awt.event.*;

/*
 *
 * GenExcept
 *
 */
public class GenExcept extends Applet
{
	// Area for user output
	TextArea output = null;

	public void init()
	{
		
		setLayout(new BorderLayout());

		Panel p = new Panel();
 		
		//
		// Feedback box. Keep a reference so
		// that we can write messages into it
		// when exceptions occur.
		//
		output = new TextArea(3, 70);
 		p.add(output);

		add("North", p);
		
		// 
		// Set up panel to hold the checkboxes 
		//
		Panel p1 = new Panel();
  		p1.setLayout(new GridLayout(8, 1));

		CheckboxGroup cbg = new CheckboxGroup();

		//
		// ToDo:
		//
		// Add checkboxes here for the various
		// exceptions you will generate. A couple
		// of samples are given.
		//
		// On each checkbox call addItemListener passing a newly created object
		// of one of the ItemListener classes you will define below
		
	    // Examples are given but
	    // you need to change the text and class names
		//
		
		Checkbox cb;
		
		p1.add(cb = new Checkbox("Your text", cbg, false));
		cb.addItemListener(new YourTextListener());
		
		p1.add(cb = new Checkbox("Other text", cbg, false));
		cb.addItemListener(new OtherTextListener());
		
		add("Center", p1);
		
    } // ends init()


    //
    //  Below we define a useful superclass from which other Listener classes
    //  can be extended
    //
    abstract class MySuperListener implements ItemListener
    {
        // All subclasses of MySuperListener must implement this method
        abstract public void throwSomething() throws Error, Exception;

        public void itemStateChanged (ItemEvent event)
        {
                if(event.getStateChange()==ItemEvent.SELECTED)
                {
                    try
                    {
    				    throwSomething();
    				    				URL u = new URL("abc://www.myco.com");
                        output.setText("No problems this time");				
					}   // ends try
					
					//
                    // ToDo:
                    //
                    // Provide catch blocks to catch:
                    // RuntimeException, Exception and Error
                    //
                    // In each catch block, output a message including
                    // the result of converting the execption to a String
                    //

					
				} // ends if
        }   // ends itemStatechanaged()				
    }   // ends member class MySuperListener            

        



    //  The sample classes shown below extend MySuperListener and
    //  override the abstract method throwSomething(). The throwSomething()
    //  method will be declared as throwing an appropriate kind of exception
    //  and the code within throwSomething should then generate that exception.
    //
    //  For Example:
    
        class MalformedURLListener extends MySuperListener
    {
        public void throwSomething () throws MalformedURLException
        {
				URL u = new URL("abc://www.myco.com");
        }
    }
    //
    //  ToDo:
    //
    //  Define other subclasses of MySuperListener -
    //  one for each of the types of
    //  Error or Exception that you would like to generate
    //
    //  The next two classes are provided as templates:
    //  change their names and add the error generating code
    //  
    
    class YourTextListener extends MySuperListener
    {
        public void throwSomething () throws Error, Exception
        {
            // Put your exception generate code here
        }
    }
    
    class OtherTextListener extends MySuperListener
    {
        public void throwSomething () throws Error, Exception
        {
            // Put your exception generating code here
        }
    }
    

} // Ends Class GenExcept
