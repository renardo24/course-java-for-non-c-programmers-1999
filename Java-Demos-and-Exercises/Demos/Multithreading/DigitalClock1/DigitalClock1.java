import java.applet.*;
import java.awt.*;
import java.util.*;

// This version doesn't work because it isn't multithreaded

public class DigitalClock1 extends Applet
{

	public void start()
	{
		while (true)
		{
			try
			{
				Thread.sleep(100);  // sleep for 100 ms.
			}
			catch (InterruptedException e)
			{}
			repaint();
		}
	}


	public void paint(Graphics g)
	{
		Font f = new Font("Helvetica", Font.BOLD, 36);
		g.setFont(f);
		Date now = new Date();
		g.setColor(Color.blue);
		g.drawString(now.toLocaleString(), 0, 50);
	}


}
