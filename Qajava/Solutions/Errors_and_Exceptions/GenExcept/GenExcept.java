import java.awt.*;
import java.applet.*;
import java.io.*;
import java.net.*;
import java.awt.event.*;

/*
 *
 * GenExcept
 *
 */
public class GenExcept extends Applet
{
	TextArea output = null;

	public void init()
	{


		setLayout(new BorderLayout());

		Panel p = new Panel();
 		
		//
		// Feedback box. Keep a reference so
		// that we can write messages into it
		// when exceptions occur.
		//
		output = new TextArea(3, 70);
 		p.add(output);

		add("North", p);
		
		// 
		// Set up panel to hold the checkboxes 
		//
		Panel p1 = new Panel();
 		p1.setLayout(new GridLayout(8, 1));

		CheckboxGroup cbg = new CheckboxGroup();
        Checkbox cb;

		p1.add(cb = new Checkbox("Malformed URL", cbg, false));
		cb.addItemListener(new MalformedURLListener());

		p1.add(cb = new Checkbox("Use null pointer", cbg, false));
		cb.addItemListener(new NullPointerListener());

		p1.add(cb = new Checkbox("Floating point divide by 0", cbg, false));
		cb.addItemListener(new FloatingPointDivdeByZeroListener());

		p1.add(cb = new Checkbox("Open invalid file", cbg, false));
		cb.addItemListener(new InvalidFileListener());

		p1.add(cb = new Checkbox("Exceed array bounds", cbg, false));
		cb.addItemListener(new ArrayBoundsListener());

		p1.add(cb = new Checkbox("Integer divide by 0", cbg, false));
		cb.addItemListener(new IntegerDivideByZeroListener());

		p1.add(cb = new Checkbox("Bad cast", cbg, false));
		cb.addItemListener(new BadCastListener());

		p1.add(cb = new Checkbox("Unknown error", cbg, false));
		cb.addItemListener(new UnknownErrorListener());

		add("Center", p1);
    }
    abstract class MySuperListener implements ItemListener
    {
        abstract public void throwSomething() throws Error, Exception;
        public void itemStateChanged (ItemEvent event)
        {
            try
            {
                if(event.getStateChange()==ItemEvent.SELECTED)
                {
    				throwSomething();
    				output.setText("That was OK!");
				}
            }
			catch (RuntimeException e)
			{
				output.setText("Got Runtime Exception: " + e);
			}
			catch (Exception e)
			{
				output.setText("Got Exception: " + e);
			}
			catch (Error e)
			{
				output.setText("Got Error: " + e);
			}
        }
    }

    class MalformedURLListener extends MySuperListener
    {
        public void throwSomething () throws Error, Exception
        {
				URL u = new URL("abc://www.myco.com");
        }
    }
    
    class NullPointerListener extends MySuperListener
    {
        public void throwSomething () throws Error, Exception
        {
			URL u = null;
			u.getContent();
        }
    }
    
    class FloatingPointDivdeByZeroListener extends MySuperListener
    {
        public void throwSomething () throws Error, Exception
        {
			double	f1 = 123.4;
			double	f2 = 0;

			double  f3 = f1/f2;
        }
    }
    
    class InvalidFileListener extends MySuperListener
    {
        public void throwSomething () throws Error, Exception
        {
			FileInputStream in = new FileInputStream("wibble_wobble");
        }
    }
    
    class ArrayBoundsListener extends MySuperListener
    {
        public void throwSomething () throws Error, Exception
        {
			int[]	array = new int[5];

			array[6] = 17;
        }
    }
    
    class IntegerDivideByZeroListener extends MySuperListener
    {
        public void throwSomething () throws Error, Exception
        {
			int	i1 = 78;
			int i2 = 0;

			int i3 = i1/i2;
        }
    }
    
    class BadCastListener extends MySuperListener
    {
        public void throwSomething () throws Error, Exception
        {
			Object o = new Object();
			Frame fr = new Frame();

			o = (Object)fr;
			String b = (String)o;
        }
    }
    
    class UnknownErrorListener extends MySuperListener
    {
        public void throwSomething () throws Error, Exception
        {
			throw new UnknownError();
        }
    }
    

}
