import java.applet.*;
import java.awt.*;

public class RandomNumberGenerator extends Applet
{
	private Button generate;
	private Label random;

	public void init()
	{
		add(new Label("Random Number Generator"));
		add(generate = new Button("Generate"));
		add(random = new Label("         "));
	}


	public boolean action(Event evt, Object arg)
	{
		boolean result = false;
		if (evt.target == generate)
		{
			int randomNum = (int)(10000 * Math.random());
			random.setText("" + randomNum);
			result = true;  // action handled
		}
		return result;
	}


}