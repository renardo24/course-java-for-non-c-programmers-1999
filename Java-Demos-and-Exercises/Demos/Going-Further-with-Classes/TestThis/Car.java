public class Car
{

	private int speed;
	private int cruiseSpeed;

	public void setCruiseSpeed(int cruiseSpeed)
	{
		this.cruiseSpeed = cruiseSpeed;
		
	}


	public void accelerate()
	{
		speed += 5;
	}


	public void attainCruiseSpeed()
	{
		speed = cruiseSpeed;
	}

	
	public int getSpeed()
	{
		return speed;
	}

}
