import java.applet.*;
import java.awt.*;

class LightBulb extends Canvas implements Switchable
{
	boolean on = false;
	
	public void on()
	{
		on = true;
		repaint();
	}
	
	public void off()
	{
		on = false;
		repaint();
	}

	public void paint(Graphics g)
	{
		g.setColor(Color.yellow);
		if (on)
			g.fillOval(50, 50, 100, 100);
		else
			g.drawOval(50, 50, 100, 100);
	}

}

