public class TestCar
{

	public static void main(String[] args)
	{
		Car car = new Car();

		Radio mr = new Radio("Sony", "CDX-3100");
		System.out.println("The volume of the radio is " + mr.getVolume());

		car.addRadio(mr);

		System.out.println("The volume of the radio is now " + mr.getVolume());
	}

}
