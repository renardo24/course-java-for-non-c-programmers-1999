public class CurrentAccount extends BankAccount
{

	public CurrentAccount(String name, float balance)
	{
		super(name, balance);
	}

	public CurrentAccount(String name)
	{
		super(name, 0);
	}


	public void addInterest()
	{
		// nothing to do
	}


	public boolean withdraw(float amount)
	{
		if (getBalance() >= amount)
		{
			setBalance(getBalance() - amount);
			return true;
		}
		else
			return false;
	}

}