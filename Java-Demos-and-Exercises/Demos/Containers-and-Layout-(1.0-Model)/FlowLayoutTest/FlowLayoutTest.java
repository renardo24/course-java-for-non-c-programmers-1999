import java.applet.*;
import java.awt.*;

// Single container with flow layout

public class FlowLayoutTest extends Applet
{

	public void init()
	{
		setBackground(Color.white);
		setLayout(new FlowLayout(FlowLayout.LEFT));

		// Default for applet or panel is flow layout
		add(new Button("Red"));
		add(new Button("Green"));
		add(new Button("Blue"));
		add(new Button("White"));
		add(new Button("Black"));
	}


	public boolean action(Event evt, Object arg)
	{
		boolean result = false;
		if (evt.target instanceof Button)
		{
			changeColor((String)arg);
			result = true;
		}
		return result;
	}

	private void changeColor(String s)
	{
		if (s.equals("Red")) setBackground(Color.red);
		else if (s.equals("Green")) setBackground(Color.green);
		else if (s.equals("Blue")) setBackground(Color.blue);
		else if (s.equals("White")) setBackground(Color.white);
		else if (s.equals("Black")) setBackground(Color.black);
		repaint();
	}

}
