import java.applet.*;
import java.awt.*;

// Creates single counter thread

public class Counter1 extends Applet implements Runnable
{
	Thread thread;
	int count;
	boolean suspended;

	public void start()
	{
		if (thread == null)
		{
			count = 0;
			thread = new Thread(this);
			thread.start();
		}
	}


	public void run()
	{
		while (true)
		{
			count++;
			repaint();
			try
			{
				Thread.sleep(100);
			}
			catch (InterruptedException e)
			{}
		}
	}


	public void paint(Graphics g)
	{
		g.drawString("Count = " + count, 50, 25);
	}


	public void stop()
	{
		thread.stop();
		thread = null;
	}


	public boolean mouseDown(Event e, int x, int y)
	{
		if (suspended)
			thread.resume();
		else
			thread.suspend();
		suspended = !suspended;
		return true;
	}

}
