import java.awt.*;

// Subclass TextArea to simulate "teletext" (or teletype)

public class TeleText extends TextArea
{

	public TeleText(int rows, int cols)
	{
		super(rows, cols);
	}

	// Write text at 10 cps.
	// Note this method must be synchronized.

	public synchronized void appendText(String str)
	{
		for (int i = 0; i < str.length(); i++)
		{
			super.appendText("" + str.charAt(i));
			try
			{
				Thread.sleep(100);  // 1/10 sec.
			}
			catch (InterruptedException e)
			{}
		}
	}

	// Simulate form feed before writing text.
	// Note this method must be synchronized.

	public synchronized void setText(String str)
	{
		for (int i = 0; i < getRows(); i++)
		{
			super.appendText("\n");
			try
			{
				Thread.sleep(100);  // 1/10 sec.
			}
			catch (InterruptedException e)
			{}
			super.appendText(str);
		}

	}


}

