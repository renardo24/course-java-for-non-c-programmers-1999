import java.io.*;
import java.net.*;

public class SimpleEchoServer
{
	public static void main(String[] args )
	{
		try 
		{
			ServerSocket listeningSocket = new ServerSocket(8189);
			Socket socket = listeningSocket.accept( );

			DataInputStream in = new DataInputStream(socket.getInputStream());
			PrintStream out = new PrintStream(socket.getOutputStream());

			out.println( "Hello! Enter \"BYE\" to quit.\r\n" );

			boolean done = false;

			while (!done)
			{
				String string = in.readLine();
				if (string == null)
					done = true;
				else
				{
					out.println("Echo: " + string + "\r\n");
					if (string.trim().equals("BYE"))
						done = true;
				}
			}
			socket.close();
		}
		catch (Exception e)
		{
			System.out.println("Exception " + e);
		}
	}
}


