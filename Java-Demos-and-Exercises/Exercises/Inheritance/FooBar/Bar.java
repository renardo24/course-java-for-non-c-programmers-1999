public class Bar extends Foo
{
	public int b = 1;

	public Bar()
	{
		b += f;
	}


	public int doIt()
	{
		return super.doIt() * b;
	}


	public static void main(String[] args)
	{
		Foo afoo = new Bar();
		System.out.println(afoo.doIt());
	}

}