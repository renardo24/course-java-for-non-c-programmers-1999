import java.applet.Applet;
import java.awt.*;

/*
 *
 * SimpleApplet
 *
 */
public class SimpleApplet extends Applet
{
	String s = "Default string";

	public void init()
	{
		// 
		// Get parameter
		//
		String str = getParameter("MyMessage");

		if (str	!= null)
		{
			s = str;
		}

}

	public void paint(Graphics g)
	{
		g.drawString("SimpleApplet: " + s, 1, 10);
	}

}

