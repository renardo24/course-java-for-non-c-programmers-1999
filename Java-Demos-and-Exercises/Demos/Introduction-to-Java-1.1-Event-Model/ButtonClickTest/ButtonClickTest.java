import java.applet.*;
import java.awt.*;
import java.awt.event.*;


public class ButtonClickTest extends Applet implements ActionListener
{
	TextField text;
	Button button;

	public void init()
	{
		button = new Button("Click me!");
		button.addActionListener(this);
		add(button);
	
		text = new TextField(20);
		add(text);
	}

	public void actionPerformed(ActionEvent e)
	{
		text.setText("You clicked the button!");
	}

}
