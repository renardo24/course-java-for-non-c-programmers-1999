import java.applet.*;
import java.awt.*;
import java.awt.event.*;

// Single container with flow layout

public class FlowLayoutTest extends Applet implements ActionListener
{

	public void init()
	{
		Button b;

		setBackground(Color.white);
		setLayout(new FlowLayout(FlowLayout.LEFT));

		add(b = new Button("Red"));
		b.addActionListener(this);

		add(b = new Button("Green"));
		b.addActionListener(this);

		add(b = new Button("Blue"));
		b.addActionListener(this);

		add(b = new Button("White"));
		b.addActionListener(this);

		add(b = new Button("Black"));
		b.addActionListener(this);

	}


	public void actionPerformed(ActionEvent evt)
	{
		changeColor(evt.getActionCommand());
	}


	private void changeColor(String s)
	{
		if (s.equals("Red")) setBackground(Color.red);
		else if (s.equals("Green")) setBackground(Color.green);
		else if (s.equals("Blue")) setBackground(Color.blue);
		else if (s.equals("White")) setBackground(Color.white);
		else if (s.equals("Black")) setBackground(Color.black);
		repaint();
	}

}
