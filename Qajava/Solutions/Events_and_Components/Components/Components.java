import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;

/*
 *
 * Components
 *
 */

public class Components extends Applet
                        implements ActionListener
{
	TextArea	feedback;
	TextField	name;
	Choice		relation;
	Button      ok, reset;

	public void init()
	{
		//
		// ToDo:
		//
		// Create a non-editable text area of
		// 5 lines by 40 columns. This is used
		// to display messages to the user.
		//
		// Initialise it with an appropriate
		// message and assign the new TextArea to
		// the instance variable named feedback.
		//
		feedback = new TextArea(5, 40);
		feedback.setText("This is the feedback text area");
		feedback.setEditable(false);

      	//
		// ToDo:
		//
		// Create a Label to display the text "Name",
		// and a TextField for the user to
		// type a name into. Assign the TextField
		// to the instance variable called name.

		Label namePrompt = new Label("Name: ");
		name = new TextField(30);

		//
		// ToDo:
		//
		// Create a Label to display the text "Relationship".
		//
		Label relationPrompt = new Label("Relationship: ");

		//
		// ToDo:
		//
		// Assign a new Choice object to the
		// relation instance variable. Add the possible
		// relationship types to the Choice. Examples would be:
		//
		//	Father, Mother, Sister, Brother,
		// 	Son, Daughter, Niece, Nephew
		//
		relation = new Choice();

		relation.addItem("Father");
		relation.addItem("Mother");
		relation.addItem("Sister");
		relation.addItem("Brother");
		relation.addItem("Son");
		relation.addItem("Daughter");
		relation.addItem("Niece");
		relation.addItem("Nephew");



		//
		// ToDo:
		//
		// Assign two new Buttons to the instance variables
		// ok and reset. Label the Buttons appropriately.
		//
		ok = new Button("OK");
		reset = new Button("Reset");

		//
		// ToDo:
		//
		// Add all the components to the applet.
		//
		add(feedback);
		add(namePrompt);
		add(name);
		add(relationPrompt);
		add(relation);
		add(ok);
		add(reset);

		//
		// ToDo:
		//
		// Register the applet as an ActionListener
		// for the two Buttons ok and reset.
		//
		ok.addActionListener(this);
		reset.addActionListener(this);
	}

    public void actionPerformed(ActionEvent evt){

		//
		// ToDo:
		//
		// Discover if the Event Source was ok or reset.
		//
		// If the source was the ok Button, check that
		// the user typed a name in the TextField.
		// If so say "Hi" to the named relation
		// by displaying a message in the TextArea
		// (e.g. "Hi there Uncle Albert").
		// Otherwise, display an error message.
		//
		if(evt.getSource() == ok){
	        if (name.getText().length() == 0)
				{
					feedback.setText("Please fill in a name");
				}
			else
				{
					feedback.setText("Hi there ");
					feedback.append(relation.getSelectedItem());
					feedback.append(" " + name.getText());
			    }
		}
		//
		// ToDo:
        //
		// If the event source was the reset Button,
		// clear the TextField and reset the Choice
		// relationship to its original value.
		// Show a message prompting the user to start again.
        //
        else if(evt.getSource() == reset){
   				name.setText("");
				relation.select(0);
				feedback.setText("Try again");
        }
	} // ends actionPerformed()

}

