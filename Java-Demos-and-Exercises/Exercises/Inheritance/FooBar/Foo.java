public class Foo
{
	public int f = 1;

	public Foo()
	{
		f = 2;
	}

	public int doIt()
	{
		return (f * 7);
	}

}
