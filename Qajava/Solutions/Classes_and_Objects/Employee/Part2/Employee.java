/* Class for employee */
 
public class Employee
{
	// Variables to hold employee's name and age
	private String name;
    private int age;

	//
	// Constructor that takes a String and an int argument
	// to initialise the instance variables for the employee's name
	// and age, respectively.
	//
    public Employee(String n, int a)
    {
		name = n;
		age  = a;
	}


	//
	// The getName method returns the employee's name
	//
    public String getName()
    {
		return name;
	}

	//
	// The getAge method returns the employee's age
	//
    public int getAge()
    {
		return age;
	}


	//
	// The incAge method increments the employee's age
	//
    public void incAge()
    {
		// Do a sanity check
		if (age < 65)
			age++;
	}


}
