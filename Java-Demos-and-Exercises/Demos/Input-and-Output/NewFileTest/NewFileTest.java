import java.io.*;

public class NewFileTest
{

	public static void main(String[] args)
	{
		File f = new File("d:\\temp", "myfile.dat");

		if (f.exists())
		{
			System.out.println("File already exists");
		}
		else
		{
			System.out.println("Creating new file");

			// create new file by creating new output stream
			try
			{
				FileOutputStream fout = new FileOutputStream(f);
				fout.close();
			}
			catch (IOException e)
			{
				System.out.println("Exception: " + e);
			}
		}
	}

}


