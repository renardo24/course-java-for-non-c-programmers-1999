import java.applet.*;
import java.awt.*;
import java.util.*;

// This first version suffers from excessive flashing

public class DigitalClock2 extends Applet implements Runnable
{
	Thread thread;

	
	public void start()
	{
		if (thread == null)
		{
			thread = new Thread(this);
			thread.start();
		}
	}

	public void run()
	{
		while (true)
		{
			try
			{
				Thread.sleep(100);
			} catch (InterruptedException e)
			{}
			repaint();
		}
	}

	public void paint(Graphics g)
	{
		Font f = new Font("Helvetica", Font.BOLD, 36);
		g.setFont(f);
		Date now = new Date();
		g.setColor(getBackground());
		g.fillRect(0, 0, size().width, size().height);
		g.setColor(Color.blue);
		g.drawString(now.toLocaleString(), 0, 50);
	}

	public void stop()
	{
		thread.stop();
		thread = null;
	}

}
