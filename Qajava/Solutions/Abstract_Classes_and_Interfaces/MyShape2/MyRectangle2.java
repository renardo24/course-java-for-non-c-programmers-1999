import java.awt.*;

/*
 *
 * MyRectangle2
 *
 */

public class MyRectangle2 extends MyShape2 implements Drawable
{
	//
	// Constructor
	//
	public MyRectangle2(int left, int top, int width, int height)
	{
		super(left, top, width, height);
	}


	//
	// Must implement draw method defined in Drawable interface
	//
	public void draw(Graphics g)
	{
		//
		// Draw the rectangle
		//
		g.drawRect(left,top,width,height);
	}
}
