import java.io.*;

// This version uses FileReader instead of FileInputStream.
// An InputStreamReader is used to convert byte stream from
// System.in into a character stream.

public class FileReaderTest
{

	public static void main(String[] args) throws IOException
	{
		Reader in;

		if (args.length == 1)
			in = new FileReader(args[0]);
		else
			in = new InputStreamReader(System.in);

		int ch;
		int nChars = 0;
		int nSpaces = 0;

		for (; (ch = in.read()) != -1; nChars++)
		{
			if (Character.isSpaceChar((char)ch))	// isSpace() is deprecated
				nSpaces++;
		}
		System.out.println("Counted " + nChars + " characters, including "
								+ nSpaces + " white spaces");
	}

}
