import java.util.*;
import java.text.*;

// This class uses some of the methods of the Calendar, Locale
// and DateFormat classes introduced in Java 1.1

public class CalendarTest
{

	public static void main(String[] args)
	{
		TimeZone tz = TimeZone.getDefault();

		// get default locale (just for interest)
		Locale loc = Locale.getDefault();
		System.out.println("Default locale is " + loc.getDisplayName());

		// get Calendar object for the default locale
		// that represents current time and date
		Calendar calendar = Calendar.getInstance();

		// get current time out of Calendar object
		Date today = calendar.getTime();

		// format time for UK locale in long format
		DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, Locale.UK);
		System.out.println(df.format(today));
 
		// see what day of the week it is (just for interest)
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

		if (dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY)
			System.out.println("It's the weekend :-)");
		else
			System.out.println("It's a weekday :-(");


	}

}