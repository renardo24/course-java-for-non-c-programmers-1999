public class StreetCred
{
	public int measureStreetCred(String model)
	{
		int streetCred = 50;

		if (model.charAt(model.length() - 1) == 'i')
			streetCred += 20;
		if (model.equals("Golf GTi"))
			streetCred += 20;
		if (model.substring(0, 5).equals("Skoda"))
			streetCred -= 30;

		return streetCred;
	}

	public static void main(String[] args)
	{
		String[] cars = {"BMW 323i", "Skoda Favorit", "Golf GTi"};

		StreetCred sc = new StreetCred();

		for (int i = 0; i < cars.length; i++)
		{
			int streetCred = sc.measureStreetCred(cars[i]);
			System.out.print("\nThe " + cars[i] + " has a street credibility of "
									+ streetCred + "%\n");
		}
	}

}
			
