import java.applet.*;
import java.awt.*;

public class TemperatureConverter extends Applet
{
	private Button convert, reset;
	private TextField degC, degF;

	public void init()
	{
		add(new Label("Temperature Converter"));
		add(new Label("Temperature in deg C"));
		add(degC = new TextField(3));  // 3 columns
		add(new Label("Temperature in deg F is "));
		add(degF = new TextField(3));
		add(convert = new Button("Convert"));
		add(reset = new Button("Reset"));
		degC.requestFocus();
		degF.setEditable(false);
	}


	public boolean action(Event evt, Object arg)
	{
		boolean result = false;
		if (evt.target == convert || evt.target == degC)
		{
			int temp = Integer.parseInt(degC.getText());
			degF.setText("" + (temp * 9/5 + 32.0F));
			result = true;        // action handled
		}
		else if (evt.target == reset)
		{
			degF.setText("");     // clear text fields
			degC.setText("");
			degC.requestFocus();  // return focus to entry field
			result = true;        // action handled
		}
		return result;
	}


}
