import java.awt.*;

public final class SwitchPanel extends Panel
{
	private Switchable targetObject = null;

	public SwitchPanel(Switchable target)
    {
		targetObject = target;

		add(new Button("On"));
		add(new Button("Off"));
    }


	public boolean action(Event event, Object obj)
	{
		if (event.target instanceof Button)
		{
			if (targetObject != null)
			{
				Button buttonTarget = (Button)event.target;
				String buttonText = buttonTarget.getLabel();

				// Test methods of the target object
				if (buttonText.equals("On"))
				{
					targetObject.on();
				}
				else if (buttonText.equals("Off"))
				{
					targetObject.off();
				}
				return true;
			}
		}
		return false;
	}


}
