import java.awt.*;
import java.awt.event.*;


// Display an empty frame window - now converted to use Java 1.1 event model with inner class, so
// Now uses member inner class, so no need to implement WindowListener interface

public class SimpleFrameWithAdapter extends Frame
{
	public SimpleFrameWithAdapter()
	{
		setSize(300, 200);        // resize() has been deprecated
		show();

		// register as a window listener
		WindowEventHandler handler = new WindowEventHandler();
		addWindowListener(handler);
	
	}

	// member inner class to handlke window events
	private class WindowEventHandler extends WindowAdapter
	{
		// must handle window-closing event to allow user to quit application
		public void windowClosing(WindowEvent we)
		{
			System.exit(0);
		}

		// no need to override any other methods
	}

	public static void main(String[] args)
	{
		Frame f = new SimpleFrameWithAdapter();
	}

}
