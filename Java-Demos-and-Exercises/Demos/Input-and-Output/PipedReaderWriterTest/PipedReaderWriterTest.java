import java.io.*;

// Converted to Java 1.1, so now uses PipedReader and PipedWriter classes

public class PipedReaderWriterTest
{
  public static void main(String args[]) throws Exception
  {
    PipedReader in = new PipedReader();
    PipedWriter out = new PipedWriter(in);

    Producer p = new Producer(out);
    Consumer s = new Consumer(in);
    Thread t1 = new Thread(p);
    Thread t2 = new Thread(s);
    t1.start();
    t2.start();
  }

}


class Producer implements Runnable
{
  Writer out;

  public Producer(Writer out)
  {
    this.out = out;
  }

  public void run() {
    byte value;

    try
	{
      for (int i = 0; i < 5; i++)
	  {
        value = (byte)(Math.random() * 256);
        out.write(value);
        System.out.println("Wrote " + value + ".. ");
        Thread.sleep((long)(Math.random() * 1000));
      }
      out.close();
    }
    catch (Exception e)
	{
      e.printStackTrace();
    }
  }

}


class Consumer implements Runnable
{
  Reader in;

  public Consumer(Reader in)
  {
    this.in = in;
  }

  public void run()
  {
    int value;

    try
	{
      while ((value = in.read()) != -1) {
        System.out.println("Read " + (byte)value);
    }
      System.out.println("Pipe closed");
    }
    catch (Exception e)
	{
      e.printStackTrace();
    }
  }

}
