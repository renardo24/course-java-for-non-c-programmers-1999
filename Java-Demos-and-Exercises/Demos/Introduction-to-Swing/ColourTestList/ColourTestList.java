import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import com.sun.java.swing.*;
import com.sun.java.swing.event.*;

// This JFC version uses a JList instead of a List component.
// Note that JList doesn't have any methods to add or remove
// items to list after it has been created and that it
// generates ListSelectionEvents rather than ItemEvents or ActionEvents.
// Also note that JList doesn't provide a scroll bar, so it's
// usually added to a JScrollPane.

public class ColourTestList extends JApplet implements ListSelectionListener
{
    private final String[] colourNames = {"Red", "Green", "Blue", "White", "Black"};

	private JList colourList;
	private Container cp;

	public void init()
	{
	    cp = getContentPane();
	    cp.setLayout(new FlowLayout());

	    cp.add(new JLabel("Background colour:"));
	    colourList = new JList(colourNames);
	    JScrollPane sp = new JScrollPane(colourList);
	    cp.add(sp);

        colourList.setVisibleRowCount(4);
        colourList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); // not the default
		colourList.setSelectedValue("White", false);  // select white as initial colour
		cp.setBackground(Color.white);

		colourList.addListSelectionListener(this);
	}


	public void valueChanged(ListSelectionEvent evt)
	{
		if (evt.getSource() instanceof JList)
		{
		    JList list = (JList)evt.getSource();
		    if (!evt.getValueIsAdjusting())
		    {
    			//changeColor((String)list.getSelectedValue());
	    		changeColor(colourNames[evt.getFirstIndex()]);
	    	}
		}
	}


	private void changeColor(String s)
	{
		if (s.equals("Red")) cp.setBackground(Color.red);
		else if (s.equals("Green")) cp.setBackground(Color.green);
		else if (s.equals("Blue")) cp.setBackground(Color.blue);
		else if (s.equals("White")) cp.setBackground(Color.white);
		else if (s.equals("Black")) cp.setBackground(Color.black);
		repaint();
	}

}
