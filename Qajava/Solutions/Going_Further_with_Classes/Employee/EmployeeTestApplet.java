import java.applet.*;
import java.awt.*;

/*
 * EmployeeTestApplet class
 *
 * EmployeeTest is a Java applet for testing
 * the methods of the Employee class. Results are
 * displayed in a tabular format using a custom
 * component called QALiveTable.
 *
 * The Employee objects should be created in
 * the init() method
 *
 * You can use the displayEmployees() method to
 * display details of the employees in the applet
 *
 * 
 * You can use the action() method to test the
 * mutator methods of the Employee class
 *
 * 
 * You can use the testX() methods to test the
 * methods of your Employee class.
 *
 */

public class EmployeeTestApplet extends Applet implements Testable
{
	// Reference to QALiveTable object that will display results
	QALiveTable table ;

	// Declare two Employee variables
	private Employee employee1, employee2;


	//
	// The init() method is called when the applet is initialised
	//
    public void init()
    {
		// Set retirement age of employees
		Employee.setRetirementAge(65);

		//
		// Create two new Employee objects
		//
        //employee1 = new Employee("Smith", 28);
        //employee2 = new Employee("Jones", 25);
		//
		// Use overloaded constructor to specify employees' full names 
        employee1 = new Employee("Andy", "Smith", 28);
        employee2 = new Employee("Maria", "Jones", 25);


		// Initialise the table for displaying the employee's details
		//
		initialiseDisplay(3, 2, 5); // 3 rows, 2 columns, 5 buttons
		displayHeadings("Employee", "Age");

		//
		// Display the employee's details in the applet
		//
		displayEmployees(employee1, employee2);

}


	//
	// This method displays record of each employee
	//
    void displayEmployees(Employee e1, Employee e2)
	{
		// Update employee 1 in row 1
		//
		if (e1 != null)
			updateEmployee(1, e1.getName(), e1.getAge());

		// Update employee 2 in row 2
		//
		if (e2 != null)
			updateEmployee(2, e2.getName(), e2.getAge());

	}


	//
	// This private method updates employee details in specified row
	//
	private void updateEmployee(int row, String name, int age)
	{
		table.setRow(row, name, "" + age);
	}



	//
	// The following testX() methods implement the Testable interface
	// and will be called by the QALiveTable component in response
	// to button clicks.  You can use them to call methods in your
	// Employee class.
	//

	public void test1()
	{
		// Call method of employee 1
		employee1.incAge();

		// Re-display employees' details
		displayEmployees(employee1, employee2);
	}

	public void test2()
	{
		// Call method of employee 2
		employee2.incAge();

		// Re-display employees' details
		displayEmployees(employee1, employee2);
	}

	public void test3()
	{
		// Call another method of employee 2
		employee2.changeName("Smith");

		// Re-display employees' details
		displayEmployees(employee1, employee2);
	}

	public void test4()
	{
		// Call another method of employee 2
		employee2.changeName("Mary", "Smith");

		// Re-display employees' details
		displayEmployees(employee1, employee2);
	}

	public void test5()
	{
		// Call class method of Employee class
		Employee.setRetirementAge(60);

		// Re-display employees' details		
		displayEmployees(employee1, employee2);
	}



	//
	// The following methods set up the QALiveTable
	// component. You do not need to modify either
	// of them.
	//

	//
	// This private method simply calls the appropriate
	// method in the QALiveTable control to set up the
	// column headings
	//
	private void displayHeadings(String s1, String s2)
	{
		table.setColumnHeadings(s1, s2);
	}


	//
	// This private method creates a QALiveTable component
	// and adds it to the applet container
	//
	private void initialiseDisplay(int rows, int cols, int buttons)
	{
		//
		// Create the QALiveTable component for displaying the results.
		// Pass this as last argument to make this
		// class the "target" object of the control panel,
		// i.e. the object that implements the Testable
		// interface.
		//
		table = new QALiveTable(rows, cols, buttons, this);

		// Ensure QALiveTable component fills applet container
		setLayout(new GridLayout(1, 1));

		// Add the QALiveTable component to applet container
		add(table);
	}

}
