import java.awt.*;

// Implement thread by subclassing Thread class

public class Counter3 extends Thread
{
	private TextField txt;
	private int count;

	public Counter3(TextField txt)
	{
		this.txt = txt;
	}
		
	public void run()
	{
		while (true)
		{
			count++;
			txt.setText("Count = " + count);
			try
			{
				Thread.sleep(100);
			}
			catch (InterruptedException e)
			{}
		}
	}

}