import java.net.*;

// This does compile!

public class Test
{

	public static void main(String[] args)
	{
		URL u1;
		MyClass mc = new MyClass();
		try
		{
			u1 = new URL("http://www.sun.com");
		}
		catch (MalformedURLException e)
		{
			System.out.println("Exception: " + e);
			System.exit(0);
		}
		URL u2 = mc.changeURL(u1);
	}

}
