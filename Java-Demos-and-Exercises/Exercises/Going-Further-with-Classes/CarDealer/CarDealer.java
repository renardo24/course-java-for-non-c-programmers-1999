public class CarDealer
{
	public static void changeCar(Car c)
	{
		c = new Car("Audi");
	}

	public static void main(String[] args)
	{
		System.out.println("\nCreating a new car ...\n");
		Car myCar = new Car("Ford");

		System.out.println("My car is a " + myCar.model);

		System.out.println("\nChanging car ...\n");
		changeCar(myCar);

		System.out.println("My car is now a " + myCar.model);
	}
}


