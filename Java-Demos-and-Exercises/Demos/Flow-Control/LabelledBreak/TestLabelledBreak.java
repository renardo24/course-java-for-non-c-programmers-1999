public class TestLabelledBreak
{

	public static void main(String[] args)
	{
		outer_loop:
		for (int i = 0; i < 10; i++)
		{
			for (int j = 0; j < 5; j++)
			{
				System.out.println("i = " + i + ", j = " + j);
				if (i+ j > 7)
					break outer_loop;
			}
		}
		System.out.println("End of program");
	}

}
