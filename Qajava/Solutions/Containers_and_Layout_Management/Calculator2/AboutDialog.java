import java.awt.*;
import java.awt.event.*;

public class AboutDialog extends Dialog
{

	public AboutDialog(Frame parent)
	{
		super(parent, "About Calculator", true);  // modal

		// Create a panel to hold text
		Panel textPanel = new Panel();
		textPanel.add(new Label("Calculator 2.0"));
		add("Center", textPanel);

		// Create a second panel for the OK button
		Panel buttonsPanel = new Panel();
		Button okButton = new Button("OK");

        okButton.addActionListener
		(
		    new ActionListener()
		    {
		        public void actionPerformed(ActionEvent evt)
		        {
		            dispose();
		        }
		    }
	    );


        addWindowListener
		(
		    new WindowAdapter()
		    {
		        public void windowClosing(WindowEvent evt)
		        {
		            dispose();
		        }
		    }
	    );

		buttonsPanel.add(okButton);
		add("South", buttonsPanel);

		resize(200, 100);
	}

/*
	//
	// Override the action() method to allow user
	// to dismiss dialog through OK button.
	//

	public boolean action(Event evt, Object arg)
	{
		if (arg.equals("OK"))
		{
			dispose();
			return true;
		}
		return false;
	}
*/
/*
    //
	// Override the handleEvent() method to allow user
	// to dismiss dialog through system menu or close button.
	//
	
	public boolean handleEvent(Event evt)
	{
		if (evt.id == Event.WINDOW_DESTROY)
		{
			dispose();
			return true;
		}
		return super.handleEvent(evt);
	}
*/

}