public class TestCar
{

	public static void main(String[] args)
	{
		Car car = new Car();

		byte gear = 5;

		System.out.println("Selecting gear " + gear);
		car.selectGear((byte)5);

		System.out.println("Our gear is now " + gear);
	}

}
