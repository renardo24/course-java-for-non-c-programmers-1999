import java.applet.*;
import java.awt.*;

public class SpotMover extends Applet implements Runnable {

int screenWidth, screenHeight;      // the Applet's dimensions (do not alter)
int spotDiameter =20;               // the diameter of the spot

int x = 0;                          // the x-coordinate for the spot         
int y = 0;                          // the y-co-ordinate for the spot


// The method setNewPosition() is called repeatedly in an infinite loop.
// By using this method to increment and decrement the values of the
// coordinate variables "x" and "y" you can change the trajectory of
// the spot as it moves through the screen.
//
// The code currently in setNewPosition increments x and y
// until the spot has reached the bottom right-hand corner of the screen.
// 
public void run()
{
    //
    // ToDo:
    //
    // Use control structures to change the values of x and y
    // so that the spot takes a different path around the screen   
    for(int i = 0; i < (screenWidth -x);i++){        
            x += 2;
            y += 1;
            paintAndPause(20);      // repaint screen and pause for 20 milliseconds
        }
        
    paintAndPause(100);             // repaint screen and pause for 50 milliseconds

    y +=  (spotDiameter * 2);
    
    paintAndPause(10);              // repaint screen and pause for 10 milliseconds    
    

}





/* ******* You do not need to change the code below this point. ***** */ 



public void paintAndPause(int delay){             
       repaint();
       try{
            Thread.sleep(delay);
       }catch(InterruptedException IE){}
}




public  void paint(Graphics g)
{
       g.setColor(Color.cyan);      
       g.fillOval(x,y,spotDiameter,spotDiameter);
}


public void init()
{
   setBackground(Color.black);
   setForeground(Color.white);
   screenWidth = getSize().width;
   screenHeight = getSize().height;
}
   
public void start()
{
    if(thread == null){
        thread = new Thread(this);        
        thread.start();
    }
}
public void stop()
{
    if(thread != null){
        thread.stop();
        thread = null;
    }
}

private Thread thread; 
    
}