import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;

/*
 *
 * Buttons
 *
 */


//
// Initial version with default layout and no panels
//

public class Buttons extends Applet
{
	Button b1, b2;
	TextField text;

	int count = 0;

	public void init()
	{
		//
		// Create the text field
		//
		text = new TextField("0", 3);

		//
		// Create the buttons
		//
		b1 = new Button("Increment");
		b2 = new Button("Decrement");


        // For each Button, use an anonymous class
        // to create an implementation of ActionListener
        // which increments or decrements the count
        // and register it with the Button

		b1.addActionListener(new ActionListener()
		{
		    public void actionPerformed(ActionEvent evt)
		    {
                // increment the count and set the text of the Label to (""+count)
				count++;
				text.setText("" + count);
		    }
		});


        b2.addActionListener(new ActionListener()
		{
		    public void actionPerformed(ActionEvent evt)
		    {
                // increment the count and set the text of the Label to (""+count)
				count--;
				text.setText("" + count);
		    }
		});

		//
		// Finally, add the components to the applet
		//
		add(text);
		add(b1);
		add(b2);

    }

}