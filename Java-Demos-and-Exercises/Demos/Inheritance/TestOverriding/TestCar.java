public class TestCar
{
	public static void main(String[] args)
	{
		Engine engine = new Engine(1400);
		Brava ba = new Brava(4, engine);
		ba.start();
		ba.selectGear(1);
		
		engine = new Engine(2000);
		TurboUnit turbo = new TurboUnit();
		Bravissimo bs = new Bravissimo(2, engine, turbo);
		bs.start();
		bs.selectGear(1);
		bs.setBoost((float)1.2);

		System.out.println("\nba is doing "
								+ ba.getSpeed() + " mph");
		System.out.println("\nbs is doing "
								+ bs.getSpeed() + " mph");
	}

}


