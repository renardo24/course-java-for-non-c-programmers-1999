public class Primes
{
	private static int[] primes = {2, 3, 5, 7, 11, 13, 17, 19};

	public static void main(String[] args)
	{

		System.out.print("\nThe first " + primes.length +
								" prime numbers are: ");
		for (int i = 0; i < primes.length; i++)
		{
			if (i != 0) System.out.print(", ");
			System.out.print(primes[i]);
		}
		System.out.println();
	}

}
