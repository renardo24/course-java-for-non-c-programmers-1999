import java.awt.*;

public class DialogTest extends Frame implements DialogProcessor
{
	TextField feedback;

	public DialogTest()
	{
		add(new Button("Login"));
		add(feedback = new TextField(30));
		feedback.setEditable(false);
		setLayout(new FlowLayout());
	}

	public void processDialog(Dialog source, Object obj)
	{
		if (source instanceof LoginDialog)
		{
			LoginDialogInfo info = (LoginDialogInfo)obj;
			// add code here to authenticate user
			if (info.password.equalsIgnoreCase("Java"))
				feedback.setText("Welcome to Java, " + info.name);
			else
				feedback.setText("Sorry, please try again.");
		}
	}


	public boolean action(Event evt, Object arg)
	{
		Dialog dialog = null;
		if (arg.equals("Login"))
		{
			if (dialog == null)
			{
				feedback.setText("");
				dialog = new LoginDialog(this);
			}
			dialog.show();
			return true;
		}
		return false;
	}


	public boolean handleEvent(Event evt)
	{
		if (evt.id == Event.WINDOW_DESTROY)
			System.exit(0);
		return super.handleEvent(evt);
	}

	public static void main(String[] args)
	{
		Frame f = new DialogTest();
		f.resize(300, 200);
		f.show();
	}


}

