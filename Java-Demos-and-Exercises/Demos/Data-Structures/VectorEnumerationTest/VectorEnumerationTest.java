import java.util.*;

public class VectorEnumerationTest
{

	public static void main(String[] args)
	{
		// create an empty vector
		Vector items = new Vector();

		// initialise vector with references to 3 items
		items.addElement(new Item("Bread", 1));
		items.addElement(new Item("Milk", 3));
		items.addElement(new Item("Coffee", 2));


		// enumerate contents of vector
		Enumeration e = items.elements();
		while (e.hasMoreElements())
		{
			Item item = (Item)e.nextElement();  // note cast
			System.out.println(item.toString());
		}

	}

}