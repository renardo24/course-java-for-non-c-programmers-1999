import java.applet.*;
import java.awt.*;

// Using a List component

public class ColourTestList extends Applet
{
	private List colourList;

	public void init()
	{
		add(new Label("Background colour:"));
		add(colourList = new List(5, false)); // 5 rows, single selection
		colourList.addItem("Red");
		colourList.addItem("Green");
		colourList.addItem("Blue");
		colourList.addItem("White");
		colourList.addItem("Black");

		setBackground(Color.white);
		colourList.select(3);  // select white as initial colour
	}


	// action method only invoked when list item is double-clicked
	// so must override handleEvent() to respond to single clicks

	/*
	public boolean action(Event evt, Object arg)
	{
		boolean result = false;
		if (evt.target == colourList)
		{
			changeColor((String)arg);
			result = true;
		}
		return result;
	}
	*/


	// must override handleEvent method to respond to
	// single clicks on list items

	public boolean handleEvent(Event evt)
	{
		if (evt.target == colourList && evt.id == Event.LIST_SELECT)
		{
			String s = colourList.getSelectedItem();
			changeColor(s);
			return true;  // event handled
		}
		return super.handleEvent(evt);  // event not handled
	}


	private void changeColor(String s)
	{
		if (s.equals("Red")) setBackground(Color.red);
		else if (s.equals("Green")) setBackground(Color.green);
		else if (s.equals("Blue")) setBackground(Color.blue);
		else if (s.equals("White")) setBackground(Color.white);
		else if (s.equals("Black")) setBackground(Color.black);
		repaint();
	}

}
