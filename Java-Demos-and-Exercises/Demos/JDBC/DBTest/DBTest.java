import java.sql.*;

public class DBTest
{
    public static void main(String[] args)
    {
        // REGISTER DRIVER
        //
        // Do this here, or define drivers in the
        // jdbc.drivers system property
		
        try
		{
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
        }
		catch (Exception e)
		{
            System.out.println(e);
        }
		
		String userName="";
		String password="";

        // GET CONNECTION
        Connection con = null;
        try
		{
            con = DriverManager.getConnection("jdbc:odbc:ADDRESS",userName,password);
        }
		catch(Exception e)
		{
            System.out.println(e);
        }

        // GET CONNECTION WARNINGS
        SQLWarning warning = null;
        try
		{
            warning = con.getWarnings();

            while (warning != null)
			{
                System.out.println("Warning: "+warning);
                warning = warning.getNextWarning();
            }
        }
		catch (Exception e)
		{
            System.out.println(e);
        }

         // CREATE STATEMENT
         //
         // Could use a preparedStatement instead
        Statement stmt = null;
        try
		{
            stmt = con.createStatement();
        }
		catch (Exception e)
		{
            System.out.println(e);
        }

         // EXECUTE QUERY
        ResultSet results = null;
        try
		{
            results = stmt.executeQuery("SELECT * FROM ADDRESS WHERE Country= 'UK'");
        }
		catch (Exception e)
		{
            System.out.println(e);
        }

        // GET ALL RESULTS
        StringBuffer buf = new StringBuffer();
		buf.append("\n");

        try
		{
            ResultSetMetaData rsmd = results.getMetaData();
            int numCols = rsmd.getColumnCount();
            int i, rowcount = 0;

           // get column header info
            for (i=1; i <= numCols; i++)
			{
                if (i > 1) buf.append(",");
                buf.append(rsmd.getColumnLabel(i));
            }
            buf.append("\n\n");

            // break it off at 50 rows max
            while (results.next() && rowcount < 50)
			{
                // Loop through each column, getting the column
                // data and displaying

                for (i=1; i <= numCols; i++)
				{
                    if (i > 1) buf.append(",");
                    buf.append(results.getString(i));
                }
                buf.append("\n");
                rowcount++;
            }
            results.close();

            System.out.println(buf);

        }
		catch (Exception e)
		{
            System.out.println(e);
            return;
        }
    }
}