public abstract class BankAccount
{
	private String lastName;
	private float balance = 0;

	public BankAccount(String name, float balance)
	{
		this.lastName = name;
		this.balance = balance;
	}

	public BankAccount(String name)
	{
		this(name, 0);
	}

	public String getName()
	{
		return lastName;
	}
	
	public boolean deposit(float amount)
	{
		if (amount > 0)
		{
			balance += amount;
			return true;
		}
		else
			return false;
	}

	public float getBalance()
	{
		return balance;
	}

	
	protected void setBalance(float balance)
	{
		this.balance = balance;
	}


	public abstract boolean withdraw(float amount);
	public abstract void addInterest();

}