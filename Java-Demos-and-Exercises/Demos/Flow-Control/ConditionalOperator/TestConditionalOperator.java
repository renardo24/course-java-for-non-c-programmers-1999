public class TestConditionalOperator
{
	public static void main(String[] args)
	{
		int value1 = 120;
		int value2 = 105;
		int maxValue = 0;

		maxValue = (value1 > value2) ? value1 : value2;
		
		System.out.println("value1 = " + value1);
		System.out.println("value2 = " + value2);
		System.out.println("Max value is " + maxValue);
	}

}

