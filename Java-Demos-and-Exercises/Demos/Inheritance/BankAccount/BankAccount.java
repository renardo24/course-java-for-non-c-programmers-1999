public class BankAccount
{
	private String lastName;
	private float balance = 0;

	public BankAccount(String name, float balance)
	{
		this.lastName = name;
		this.balance = balance;
	}

	public BankAccount(String name)
	{
		//this.lastName = name;
		this(name, 0);
	}

	public String getName()
	{
		return lastName;
	}
	
	public boolean deposit(float amount)
	{
		if (amount > 0)
		{
			balance += amount;
			return true;
		}
		else
			return false;
	}

	public boolean withdraw(float amount)
	{
		if (balance >= amount)
		{
			balance -= amount;
			return true;
		}
		else
			return false;
	}

	public float getBalance()
	{
		return balance;
	}

	protected void setBalance(float balance)
	{
		this.balance = balance;
	}

	public static void main(String[] arg)
	{
		BankAccount account = new BankAccount("Cripps");
		account.deposit((float)1000.00);
		System.out.println("Account for " + account.getName() +
			" is " + account.getBalance());
	}

}