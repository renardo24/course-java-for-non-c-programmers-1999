import java.io.*;

// This version uses anonymous inner class, so needs Java 1.1 compiler

public class ListDirectoryWithFilterTest2
{

	public static void main(String[] args)
	{
		final String fileName, fileExtension;	// must be final to allow access
												// from inner class.

		ListDirectoryWithFilterTest2 ld = new ListDirectoryWithFilterTest2();
		
		if (args.length >= 1)
			fileName = args[0];
		else
			fileName = "d:\\temp";

		if (args.length == 2)
			fileExtension = args[1];
		else
			fileExtension = "html";


		File f = new File(fileName);
		if (f.isDirectory())
		{
			System.out.println("Directory of " + fileName + ":\n");

			String[] files = f.list(new FilenameFilter() {
				public boolean accept(File dir, String name)
				{
					boolean result = false;
					if (name.endsWith(fileExtension))
						result = true;
					return result;
				}
			} );

			for (int i = 0; i < files.length; i++)
				System.out.println(files[i]);
		}
		else
			System.out.println("Error: " + args[0] + " is not a directory");

	}

}
