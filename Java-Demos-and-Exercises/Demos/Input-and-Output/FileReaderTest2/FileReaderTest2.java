import java.io.*;

// This improved version not only uses a FileReader instead of
// a FileInputStream, but is also uses a BufferedReader for
// increased efficiency. An InputStreamReader is used to convert
// byte stream from System.in into a character stream.

public class FileReaderTest2
{

	public static void main(String[] args) throws IOException
	{
		Reader in;

		if (args.length == 1)
			in = new FileReader(args[0]);
		else
			in = new InputStreamReader(System.in);

		in = new BufferedReader(in);

		int ch;
		int nChars = 0;
		int nSpaces = 0;

		for (; (ch = in.read()) != -1; nChars++)
		{
			if (Character.isSpaceChar((char)ch))	// isSpace() is deprecated
				nSpaces++;
		}
		System.out.println("Counted " + nChars + " characters, including "
								+ nSpaces + " white spaces");
	}

}
