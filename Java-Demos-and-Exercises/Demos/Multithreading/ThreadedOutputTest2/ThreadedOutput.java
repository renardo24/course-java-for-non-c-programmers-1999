import java.awt.*;

// Output messages in separate thread

public class ThreadedOutput implements Runnable
{
	private String msg;    // the message to display
	private TextArea txt;  // where to display it


	public ThreadedOutput(String msg, TextArea txt)
	{
		this.msg = msg;
		this.txt = txt;
	}


	public void run()
	{
		while (true)
		{
			txt.setText("");
			txt.appendText(msg);
			try
			{
				Thread.sleep(1000);
			} catch (InterruptedException e)
			{}
		}

	}


}

