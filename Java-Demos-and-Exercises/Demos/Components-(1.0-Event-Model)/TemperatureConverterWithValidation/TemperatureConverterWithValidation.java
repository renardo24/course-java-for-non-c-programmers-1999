import java.applet.*;
import java.awt.*;

// With input validation

public class TemperatureConverterWithValidation extends Applet
{
	private Button convert, reset;
	private TextField degC, degF;

	public void init()
	{
		add(new Label("Temperature Converter 2"));
		add(new Label("Temperature in deg C"));
		add(degC = new TextField(3));
		add(new Label("Temperature in deg F is "));
		add(degF = new TextField(3));
		add(convert = new Button("Convert"));
		add(reset = new Button("Reset"));
		degC.requestFocus();
		degF.setEditable(false);
	}


	public boolean action(Event e, Object arg)
	{
		boolean result = false;
		if (e.target == convert || e.target == degC)
		{
			String value = degC.getText();
			try
			{
				int temp = Integer.parseInt(value) * 9/5 + 32;
				degF.setText("" + temp);
			}
			catch (NumberFormatException ex)
			{
				degF.setText("");
				degC.setText("");
				degC.requestFocus();
			}
			result = true;
		}
		else if (e.target == reset)
		{
			degF.setText("");
			degC.setText("");
			degC.requestFocus();
			result = true;
		}
		return result;
	}


}
