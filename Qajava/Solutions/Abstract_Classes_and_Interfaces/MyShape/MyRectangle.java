import java.awt.*;

/*
 *
 * MyRectangle
 *
 */

public class MyRectangle extends MyShape
{
	//
	// Constructor
	//
	public MyRectangle(int left, int top, int width, int height)
	{
		super(left, top, width, height);
	}


	//
	// Must implement draw method defined in MyShape
	//
	public void draw(Graphics g)
	{
		//
		// Draw the rectangle
		//
		g.drawRect(left,top,width,height);
	}
}
