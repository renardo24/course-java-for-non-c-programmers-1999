import java.awt.*;
import java.awt.event.*;
import java.applet.*;


// An Applet that displays Buttons and a TextField which are sensitive to
// the mouse passing over them. When the TextField gains the focus, its
// text is selected.
public class ActiveApplet extends Applet
{
    public void init()
    {
        //
        // ToDo
        //
        // Inside the Applet's init() method create a local inner class
        // called ComponentMouseListener which extends MouseAdapter.
        //
        // Give the local class two Color instance
        // fields: "enterColor" and "exitColor". Provide the class with
        // constructor which takes two Color objects as its arguments,
        // and use these parameters to initialise the instance fields.
        //

        class ComponentMouseListener extends MouseAdapter{
            private Color enterColor;
            private Color exitColor;

              public ComponentMouseListener(Color in, Color out){
                    enterColor  =in;
                    exitColor = out;
               }
        // Override the mouseEntered()method to change the colour of the
        // Component which triggers MouseEvent to "enterColor".
        //               
               public void mouseEntered(MouseEvent me)
               {
                    Component c = (Component) me.getSource();
                    c.setBackground(enterColor);
                }
        // Override the mouseExited() method to change the colour of the
        // Component which triggers MouseEvent to "exitColor".
        //                
               public void mouseExited(MouseEvent me)
               {
                    Component c = (Component) me.getSource();
                    c.setBackground(exitColor);
                }
        }
        
        //
        // ToDo:
        //
        // Create a new instance of ComponentMouseListener and assign it
        // to a MouseListener variable.
        //
        MouseListener mouse = new ComponentMouseListener(Color.magenta,Color.blue);

        //
        // ToDo:
        //
        // Use a loop to add five Buttons labelled "Press Me".
        // Register the MouseListener with each of them.
        //

        for(int i=0;i<5;i++){
            final Button b = new Button("Press Me");
            b.addMouseListener(mouse);
            add(b);
        //
        // ToDO:
        //
        // While still inside the loop, give each Button an
        // ActionListener using an anonymous inner class.
        //
        // Each ActionListener should be given an instance variable
        // named count to record the number of times its
        // actionPerformed() method has been called.
        //
        // In the actionPerformed() method, increment count,
        // change the Button's background colour to red, and
        // set the Button's label to display the count variable.
        //
            b.addActionListener(new ActionListener(){
                int count = 0;
                public void actionPerformed(ActionEvent ae)
                {
                    b.setBackground(Color.red);
                    b.setLabel("Count: " +  ++count);
                }
            });
        }

        //
        // ToDo:
        //
        // Create a new instance of ComponentMouseListener (using different colours).
        // Add a new TextField, and Register the ComponentMouseListener
        // as a MouseListener for the TextField
        //
        MouseListener mouse2 = new ComponentMouseListener(Color.yellow,Color.cyan);
        final TextField text = new TextField("Enter text here",50);
        text.addMouseListener(mouse2);
        add(text);        

        //
        // ToDo:
        //
        // Use an anonymous inner class which extends FocusAdapter
        // to select the text in the TextField when the focus is gained
        // [ java.awt.TextComponent defines a method selectAll() ]
        //
        text.addFocusListener(new FocusAdapter(){
            public void focusGained(FocusEvent fe)
            {
                text.selectAll();
            }
        });


} // ends init()


}   // ends Applet