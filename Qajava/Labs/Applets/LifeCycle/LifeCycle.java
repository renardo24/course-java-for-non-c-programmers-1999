import java.applet.Applet;

/*
 *
 * LifeCycle
 *
 */
class LifeCycle extends Applet
{
	//
	// ToDo:
	//
	// Add in override methods for all of the
	// standard methods called by the VM during 
	// the applet lifecycle.
	//
	// From each method, display a message to the
	// user to indicate that the method has been
	// called.
	//
}