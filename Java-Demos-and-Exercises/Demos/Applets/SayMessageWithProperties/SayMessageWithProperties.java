import java.applet.*;
import java.awt.*;
import java.util.*;
import java.io.*;
import java.net.*;

public class SayMessageWithProperties extends Applet
{
	private Properties message = new Properties();
	private String messageText;
	private Font font;

	public void init()
	{
		try
		{
			URL url = new URL(getDocumentBase(), "message.dat");
			InputStream in = url.openStream();
			message.load(in);
		}
		catch (Exception e)
		{
			showStatus("Exception " + e);
		}
		String fontName = message.getProperty("FontName", "Courier");
		int fontSize = Integer.parseInt(message.getProperty("FontSize", "24"));
		font = new Font(fontName, Font.BOLD, fontSize);
		messageText = message.getProperty("Message", "No message");
	}


	public void paint(Graphics g)
	{
		g.setFont(font);
		g.setColor(Color.blue);
		g.drawString(messageText, 5, 50);
	}



}
