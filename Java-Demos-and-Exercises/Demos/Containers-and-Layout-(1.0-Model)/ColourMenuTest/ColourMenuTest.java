import java.applet.*;
import java.awt.*;

public class ColourMenuTest extends Frame
{

	public ColourMenuTest()
	{
		setBackground(Color.white);

		MenuBar mb = new MenuBar();
		Menu colorMenu = new Menu("Color");

		colorMenu.add(new MenuItem("Red"));
		colorMenu.add(new MenuItem("Green"));
		colorMenu.add(new MenuItem("Blue"));
		colorMenu.add(new MenuItem("White"));
		colorMenu.add(new MenuItem("Black"));

		mb.add(colorMenu);
		setMenuBar(mb);
	}


	public boolean action(Event e, Object arg)
	{
		boolean result = false;
		if (e.target instanceof MenuItem)
		{
			changeColor((String)arg);
			result = true;
		}
		return result;
	}

	private void changeColor(String s)
	{
		if (s.equals("Red")) setBackground(Color.red);
		else if (s.equals("Green")) setBackground(Color.green);
		else if (s.equals("Blue")) setBackground(Color.blue);
		else if (s.equals("White")) setBackground(Color.white);
		else if (s.equals("Black")) setBackground(Color.black);
		repaint();
	}


	public boolean handleEvent(Event evt)
	{
		if (evt.id == Event.WINDOW_DESTROY)
			System.exit(0);
		return super.handleEvent(evt);
	}


	public static void main(String[] args)
	{
		Frame f = new ColourMenuTest();
		f.resize(200, 200);
		f.show();
	}


}
