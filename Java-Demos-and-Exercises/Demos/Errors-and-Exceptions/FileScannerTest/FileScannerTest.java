import java.io.*;

public class FileScannerTest
{

	private boolean scanFile(String file) throws IOException
	{
		FileInputStream fin = null;
		int b;

		try
		{
			System.out.println("Opening file ...");
			fin = new FileInputStream(file);
			// see if it's a printable file (should also check for new lines, etc.)
			while ((b = fin.read()) != -1)
				if (b < 0x20 || b >= 0x7f) return false;
		}
		finally
		{
			if (fin != null)
			{
				System.out.println("Closing file ...");
				fin.close();
			}
		}
		return true;
	}

	
	public static void main(String[] args)
	{
		FileScannerTest fv = new FileScannerTest();
		try
		{
			if (!fv.scanFile("test.txt"))
				System.out.println("Not a printable text file");
			else
				System.out.println("A printable text file");
		}
		catch (IOException e)
		{
			System.out.println("Exception " + e);
		}
	}


}

