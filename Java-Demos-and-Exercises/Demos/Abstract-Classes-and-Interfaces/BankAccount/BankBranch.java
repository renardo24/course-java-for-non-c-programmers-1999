public class BankBranch
{
	BankAccount[] accounts;

	public void printBalances()
	{
		System.out.println("\nName\tBalance");
		for (int i = 0; i < accounts.length; i++)
		{
			System.out.println(accounts[i].getName() + "\t" +
				                        accounts[i].getBalance());
		}
	}

	public void doEndOfMonthOps()
	{
		System.out.println("\nEnd of month operations");
		for (int i = 0; i < this.accounts.length; i++)
		{
			accounts[i].addInterest();
		}
	}


	public static void main(String[] args)
	{
		BankBranch branch = new BankBranch();

		// set interest rates
		CreditAccount.setInterestRate(10.0F);
		DepositAccount.setInterestRate(6.5F);
		DepositAccount.setThreshold(1000);

		// create accounts
		CreditAccount creAcc = new CreditAccount("Smith", -500);
		creAcc.setLimit(1000);

		DepositAccount depAcc= new DepositAccount("Jones", 1000);

		CurrentAccount curAcc = new CurrentAccount("Harris", 100);
		

		BankAccount[] accounts = { curAcc, depAcc, creAcc };
		branch.accounts = accounts;

		// simulate operations
		System.out.println("\nOpening balances");
		branch.printBalances();

		branch.doEndOfMonthOps();

		System.out.println("\nClosing balances");
		branch.printBalances();

	}


}