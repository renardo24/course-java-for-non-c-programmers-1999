import java.awt.*;

public interface DialogProcessor
{
	void processDialog(Dialog source, Object obj);
}
