import java.applet.*;
import java.awt.*;

// Using a panel to position buttons at the bottom of the applet

public class PanelLayoutTest extends Applet
{
	Panel p;

	public void init()
	{
		setBackground(Color.white);

		// Change layout to border layout
		
		setLayout(new BorderLayout());

		// Create a new panel

		p = new Panel();
		p.add(new Button("Red"));
		p.add(new Button("Green"));
		p.add(new Button("Blue"));
		p.add(new Button("White"));
		p.add(new Button("Black"));

		// Add panel to South side of applet

		add("South", p);
	}


	public boolean action(Event evt, Object arg)
	{
		boolean result = false;
		if (evt.target instanceof Button)
		{
			changeColor((String)arg);
			result = true;
		}
		return result;
	}

	private void changeColor(String s)
	{
		if (s.equals("Red")) setBackground(Color.red);
		else if (s.equals("Green")) setBackground(Color.green);
		else if (s.equals("Blue")) setBackground(Color.blue);
		else if (s.equals("White")) setBackground(Color.white);
		else if (s.equals("Black")) setBackground(Color.black);

		repaint();
	}

}


