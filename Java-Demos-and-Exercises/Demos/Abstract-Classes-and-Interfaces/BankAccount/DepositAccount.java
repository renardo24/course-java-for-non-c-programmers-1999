public class DepositAccount extends BankAccount
{
	private static float threshold;
	private static float interestRate;

	public DepositAccount(String name, float balance, float threshold)
	{
		super(name, balance);
		this.threshold = threshold;
	}

	public DepositAccount(String name, float balance)
	{
		super(name, balance);
	}

	public DepositAccount(String name)
	{
		super(name, 0);
	}


	public static boolean setThreshold(float newThreshold)
	{
		boolean result = false;

		if (newThreshold >= 0)
		{
			threshold = newThreshold;
			result = true;
		}
		return result;
	}

	public static float getThreshold()
	{
		return threshold;
	}


	// simplified version of addInterest() for demo purposes only

	public void addInterest()
	{
		float balance = getBalance();

		if (balance >= threshold)
			setBalance(balance * (1 + interestRate/100.0F));
	}


	public boolean withdraw(float amount)
	{
		if (getBalance() >= amount)
		{
			setBalance(getBalance() - amount);
			return true;
		}
		else
			return false;
	}


	public static void setInterestRate(float rate)
	{
		interestRate = rate;
	}

	public static float getInterestRate()
	{
		return interestRate;
	}
 
}