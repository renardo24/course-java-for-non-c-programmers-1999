import java.awt.*;
import java.awt.event.*;


// display an empty frame window
// updated to use Java 1.1 event model

public class SimpleFrame extends Frame implements WindowListener
{
	public SimpleFrame()
	{
		setSize(300, 200);        // resize() has been deprecated
		show();
		addWindowListener(this);  // register as a window listener
	}


	// must handle window-closing events to
	// allow user to quit application

	public void windowClosing(WindowEvent we)
	{
		System.exit(0);
	}


	// null methods for remainder of WindowListener interface

	public void windowActivated(WindowEvent we) {}

	public void windowClosed(WindowEvent we) {}

	public void windowDeactivated(WindowEvent we) {}
	
	public void windowDeiconified(WindowEvent we) {}

	public void windowIconified(WindowEvent we) {}

	public void windowOpened(WindowEvent we) {}


	public static void main(String[] args)
	{
		Frame f = new SimpleFrame();
	}

}
