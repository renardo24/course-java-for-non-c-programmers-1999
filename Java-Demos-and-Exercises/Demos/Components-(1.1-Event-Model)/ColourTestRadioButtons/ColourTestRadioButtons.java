import java.applet.*;
import java.awt.*;
import java.awt.event.*;

// using checkbox group (i.e. radio buttons)

public class ColourTestRadioButtons extends Applet implements ItemListener
{
	Checkbox red, green, blue, white, black;

	public void init()
	{
		setBackground(Color.white);

		CheckboxGroup cbg = new CheckboxGroup();

		add(red = new Checkbox("Red", cbg, false));
		red.addItemListener(this);

		add(green = new Checkbox("Green", cbg, false));
		green.addItemListener(this);

		add(blue = new Checkbox("Blue", cbg, false));
		blue.addItemListener(this);

		add(white = new Checkbox("White", cbg, true));  // initially checked
		white.addItemListener(this);

		add(black = new Checkbox("Black", cbg, false));
		black.addItemListener(this);

	}


	public void itemStateChanged(ItemEvent evt)
	{
		if (evt.getStateChange() == ItemEvent.SELECTED)
		{
			Object source = evt.getSource();

			if (source == red) setBackground(Color.red);
			else if (source == green) setBackground(Color.green);
			else if (source == blue) setBackground(Color.blue);
			else if (source == white) setBackground(Color.white);
			else if (source == black) setBackground(Color.black);

			repaint();
		}
	}

}
