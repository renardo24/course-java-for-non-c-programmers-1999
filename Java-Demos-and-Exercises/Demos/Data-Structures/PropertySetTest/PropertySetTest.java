import java.util.*;

public class PropertySetTest
{

	public static void main(String[] args)
	{
		// create an empty property set
		Properties prices = new Properties();

		// initialise property set with 3 prices
		// Note: both keys and values must be strings
		prices.put("Bread", "0.85");
		prices.put("Milk", "0.38");
		prices.put("Coffee", "1.50");

		System.out.println();

		// enumerate keys and prices held in property set
		Enumeration e = prices.propertyNames();

		while (e.hasMoreElements())
		{
			// get next key
			String key = (String)e.nextElement();  // note cast
			// now get its price
			String price = prices.getProperty(key);
			System.out.println(key + "\t" + price);
		}

		// easy way to list property set - useful for debugging
		//prices.list(System.out);


	}

}