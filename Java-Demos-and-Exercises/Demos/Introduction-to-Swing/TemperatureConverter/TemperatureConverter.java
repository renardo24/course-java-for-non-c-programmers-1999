import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import com.sun.java.swing.*;

// now uses JFC

public class TemperatureConverter extends JApplet implements ActionListener
{
	private JButton convert, reset;
	private JTextField degC, degF;

	public void init()
	{
	    Container cp = getContentPane();
	    cp.setLayout(new GridLayout(3, 2, 20, 20));
	    
		cp.add(new JLabel("Temperature in deg C: "));
		cp.add(degC = new JTextField(3));  // 3 columns max
		cp.add(new JLabel("Temperature in deg F: "));
		cp.add(degF = new JTextField(3));
		cp.add(convert = new JButton("Convert"));
		cp.add(reset = new JButton("Reset"));
		degC.requestFocus();
		degF.setEditable(false);

		convert.addActionListener(this);
		reset.addActionListener(this);
		degC.addActionListener(this);
	}


	public void actionPerformed(ActionEvent evt)
	{
		Object source = evt.getSource();
		if (source == convert || source == degC)
		{
			int temp = Integer.parseInt(degC.getText());
			degF.setText("" + (temp * 9/5 + 32.0F));
		}
		else if (source == reset)
		{
			degF.setText("");     // clear text fields
			degC.setText("");
			degC.requestFocus();  // return focus to entry field
		}
	}


}
