import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;

/*
 *
 * Buttons 3
 *
 */


//
// Third version using border layout together with
// panels to align buttons vertically on right-hand
// side of the applet.
//

public class Buttons3 extends Applet
{
	Button b1, b2;
	TextField text;

	int count = 0;

	public void init()
	{
		//
		// Set the applet to border layout
		//
		setLayout(new BorderLayout());

		//
		// Create a panel for the text field
		//
		Panel textPanel = new Panel();

		//
		// Create the text field and add it to the panel
		//
		text = new TextField("0", 3);
		textPanel.add(text);

		//
		// Create another panel for the buttons
		//
		Panel buttonsPanel = new Panel();

		//
		// Set layout of buttons panel to grid layout
		// in order to align buttons vertically.
		// Specify 3 rows, 1 column with vertical
		// gap between components of 5 pixels
		//
		//buttonsPanel.setLayout(new GridLayout(3, 1));
		buttonsPanel.setLayout(new GridLayout(3, 1, 0, 5));

		//
		// Create the buttons and add them to the panel
		//
		b1 = new Button("Increment");
		buttonsPanel.add(b1);

		b2 = new Button("Decrement");
		buttonsPanel.add(b2);

        // For each Button, use an anonymous class
        // to create an implementation of ActionListener
        // which increments or decrements the count
        // and register it with the Button


		b1.addActionListener(new ActionListener()
		{
		    public void actionPerformed(ActionEvent evt)
		    {
                // increment the count and set the text of the Label to (""+count)
				count++;
				text.setText("" + count);
		    }
		});


        b2.addActionListener(new ActionListener()
		{
		    public void actionPerformed(ActionEvent evt)
		    {
                // increment the count and set the text of the Label to (""+count)
				count--;
				text.setText("" + count);
		    }
		});


		//
		// Finally, add the text panel to the
		// center region of the applet ...
		//
		add("Center", textPanel);

		//
		// ... and add the buttons panel to the
		// east region of the applet
		//
		add("East", buttonsPanel);


	}


	//
	// Override getInsets() method to inset all components
	// by 10 pixels from edges of applet container
	//

	public Insets getInsets()
	{
		return new Insets(10, 10, 10, 10);
	}


}