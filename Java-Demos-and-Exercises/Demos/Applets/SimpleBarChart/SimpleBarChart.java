import java.awt.*;
import java.applet.*;

// Applet to display contents of an array
// as a vertical bar chart

public class SimpleBarChart extends Applet
{
	private int[] values;

	public void init()
	{
		// Create an array of ten integers
		values = new int[20];
		
		// For test purposes, initialise the array with random values
		for (int i = 0; i < values.length; i++)
			values[i] = (int)(100 * Math.random());
	}


	public void paint(Graphics g)
	{
		// Determine max value
		int maxValue  = 0;
		for (int i = 0; i < values.length; i++)
		{
			if (maxValue < values[i])
				maxValue = values[i];
		}

		// Get size of applet's rectangle
		Dimension d = size();
		int clientWidth = d.width;
		int clientHeight = d.height;

		// Calculate required width of each bar
		int barWidth = clientWidth/values.length;

		// Calculate scaling factor
		double scale = clientHeight/maxValue;

		// Draw the bars
		g.setColor(Color.red);
		for (int i = 0; i < values.length; i++)
		{
			int x1 = i * barWidth + 1;
			int y1 = (int)((maxValue - values[i]) * scale);
			int barHeight = (int)(values[i] * scale);

			g.fillRect(x1, y1, barWidth-2, barHeight);
		}

	}


}





