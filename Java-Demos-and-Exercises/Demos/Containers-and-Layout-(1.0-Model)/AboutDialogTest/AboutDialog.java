import java.awt.*;

public class AboutDialog extends Dialog
{

	public AboutDialog(Frame parent)
	{
		super(parent, "About DialogTest", true);  // modal
		setLayout(new FlowLayout());
		add(new Label("DialogTest V1.0"));
		add(new Button("OK"));
		pack();   // size dialog to preferred size of its components
		show();
	}

	public boolean handleEvent(Event evt)
	{
		if (evt.id == Event.WINDOW_DESTROY)
		{
			dispose();
			return true;
		}
		return super.handleEvent(evt);
	}

	public boolean action(Event evt, Object arg)
	{
		Dialog dialog;
		if (arg.equals("OK"))
		{
			dispose();
			return true;
		}
		return false;
	}

	


}
