public class Average
{
	public static void main(String[] args)
	{
		if (args.length == 0)
		{
			System.out.println("Usage: average <x1 x2 ...>");
			System.exit(1);
		}
		float sum = 0; 
		for(int i = 0; i < args.length; i++)
			sum += Integer.parseInt(args[i]);
		System.out.println("Average is " +
							sum / args.length);
	}

}
