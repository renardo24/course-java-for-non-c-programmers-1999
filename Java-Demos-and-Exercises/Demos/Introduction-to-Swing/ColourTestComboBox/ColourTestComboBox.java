import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import com.sun.java.swing.*;

// This JFC version uses JComboBox instead of Choice component.
// Note that JComboBox generates ActionEvents when user has
// finished making a selection.

public class ColourTestComboBox extends JApplet implements ActionListener
{
	private JComboBox colourCombo;
	private Container cp;

	public void init()
	{
	    cp = getContentPane();
	    cp.setLayout(new FlowLayout());
	    
	    cp.add(new JLabel("Background colour:"));
		cp.add(colourCombo = new JComboBox());
		colourCombo.addItem("Red");
		colourCombo.addItem("Green");
		colourCombo.addItem("Blue");
		colourCombo.addItem("White");
		colourCombo.addItem("Black");

		cp.setBackground(Color.white);
		colourCombo.setSelectedItem("White");  // select white as initial colour

		colourCombo.addActionListener(this);
	}


	public void actionPerformed(ActionEvent evt)
	{
		if (evt.getSource() instanceof JComboBox)
		{
		    JComboBox cb = (JComboBox)evt.getSource();
			changeColor((String)cb.getSelectedItem());
		}	
	}


	private void changeColor(String s)
	{
		if (s.equals("Red")) cp.setBackground(Color.red);
		else if (s.equals("Green")) cp.setBackground(Color.green);
		else if (s.equals("Blue")) cp.setBackground(Color.blue);
		else if (s.equals("White")) cp.setBackground(Color.white);
		else if (s.equals("Black")) cp.setBackground(Color.black);
		repaint();
	}

}
