import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import com.sun.java.swing.*;

// Now ported to JFC

public class ColourMenuTest extends JFrame implements ActionListener
{
    Container cp;

	public ColourMenuTest()
	{
		JMenuItem m1, m2, m3, m4, m5;

        cp = getContentPane();
		cp.setBackground(Color.white);

		JMenuBar mb = new JMenuBar();
		JMenu colorMenu = new JMenu("Color");

		colorMenu.add(m1 = new JMenuItem("Red"));
		colorMenu.add(m2 = new JMenuItem("Green"));
		colorMenu.add(m3 = new JMenuItem("Blue"));
		colorMenu.add(m4 = new JMenuItem("White"));
		colorMenu.add(m5 = new JMenuItem("Black"));

		mb.add(colorMenu);
		setJMenuBar(mb); // not setMenuBar() !!!

		m1.addActionListener(this);
		m2.addActionListener(this);
		m3.addActionListener(this);
		m4.addActionListener(this);
		m5.addActionListener(this);
	}


	public void actionPerformed(ActionEvent evt)
	{
		changeColor(evt.getActionCommand());
	}


	private void changeColor(String s)
	{
		if (s.equals("Red")) cp.setBackground(Color.red);
		else if (s.equals("Green")) cp.setBackground(Color.green);
		else if (s.equals("Blue")) cp.setBackground(Color.blue);
		else if (s.equals("White")) cp.setBackground(Color.white);
		else if (s.equals("Black")) cp.setBackground(Color.black);
		repaint();
	}


	public static void main(String[] args)
	{
		JFrame f = new ColourMenuTest();
		f.setSize(200, 200);
		f.setVisible(true);
		f.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt)
			{
			    System.exit(0);
			}
		});

	}


}
