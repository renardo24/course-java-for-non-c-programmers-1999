import java.io.*;

public class PrintStreamTest
{

	public static void main(String[] args) throws IOException
	{
		InputStream in = System.in;
		PrintStream out = System.out;
		StringBuffer sb = new StringBuffer();
		String s;
		int bytesRead;

		if (args.length == 1)
			out = new PrintStream(new FileOutputStream(args[0]));

		byte buffer[] = new byte[128];
		
		while (true)
		{
			bytesRead = in.read(buffer);
			if (bytesRead != -1)
			{
				// convert bytes into a String
				s = new String(buffer, 0, 0, bytesRead);

				// append to StringBuffer
				sb.append(s);
			}
			else
				break;
		}

		// Convert StringBuffer back into a String
		if (sb.length() != 0)
		{
			s = sb.toString();
			out.println(s);
		}

		out.close();
	}

}

