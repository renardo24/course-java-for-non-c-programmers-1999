public class TestPrecedence
{
	public static void main(String[] args)
	{
		int var1 = 0;

		var1 = 2 + 3 * 4;
		System.out.println("2 + 3 * 4 = " + "\t" + var1 + "\n");

		var1 = (2 + 3) * 4;
		System.out.println("(2 + 3) * 4 = " +  "\t" + var1 + "\n");

		var1 = 12 - 6 + 3;
		System.out.println("12 - 6 + 3 = " +  "\t" + var1 + "\n");

		var1 = 12 - (6 + 3);
		System.out.println("12 - (6 + 3) = " +  "\t" + var1 + "\n");

	}

}
