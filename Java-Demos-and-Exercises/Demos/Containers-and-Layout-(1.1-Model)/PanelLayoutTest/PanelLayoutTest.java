import java.applet.*;
import java.awt.*;
import java.awt.event.*;

// Using a panel to position buttons at the bottom of the applet

public class PanelLayoutTest extends Applet implements ActionListener
{
	Panel p;

	public void init()
	{
		Button b;

		setBackground(Color.white);

		// Change layout to border layout
		
		setLayout(new BorderLayout());

		// Create a new panel

		p = new Panel();
		p.add(b = new Button("Red"));
		b.addActionListener(this);

		p.add(b = new Button("Green"));
		b.addActionListener(this);

		p.add(b = new Button("Blue"));
		b.addActionListener(this);

		p.add(b = new Button("White"));
		b.addActionListener(this);

		p.add(b = new Button("Black"));
		b.addActionListener(this);

		// Add panel to South side of applet

		add("South", p);

	}


	public void actionPerformed(ActionEvent evt)
	{
		changeColor(evt.getActionCommand());
	}


	private void changeColor(String s)
	{
		if (s.equals("Red")) setBackground(Color.red);
		else if (s.equals("Green")) setBackground(Color.green);
		else if (s.equals("Blue")) setBackground(Color.blue);
		else if (s.equals("White")) setBackground(Color.white);
		else if (s.equals("Black")) setBackground(Color.black);

		repaint();
	}

}


