import java.awt.*;
import java.awt.event.*;

class AboutDialog extends Dialog
{
	Frame parent;
	Button okButton;

	AboutDialog(Frame  parent, String  title)
	{
		super(parent, title, true);  // modal
		this.parent = parent;

		setBounds(325,250, 150, 100);
		setLayout(new FlowLayout());

		add(okButton = new Button("OK"));
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed( ActionEvent ae )
			{
				dispose();
			}
		});

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt)
			{
				dispose();
			}
		});

	}

}