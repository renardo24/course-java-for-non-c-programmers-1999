public class TestCar
{
	public static void main(String[] args)
	{
		Car car1 = new Car();

		for (int i = 0; i < 12; i++)
			car1.accelerate();

		System.out.println("\ncar1 is now doing " + car1.getSpeed());
		
		System.out.println("\nSetting cruise speed to 55");
		car1.setCruiseSpeed(55);
		car1.attainCruiseSpeed();

		System.out.println("\ncar1 is now doing " + car1.getSpeed());		

	}

}

		