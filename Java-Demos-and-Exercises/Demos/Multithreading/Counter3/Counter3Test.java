import java.applet.*;
import java.awt.*;

// Creates two counter threads (alternative implementation)

public class Counter3Test extends Applet
{
	private TextField txt1, txt2;
	private Counter3 count1, count2;
	boolean suspended;

	public void init()
	{
		txt1 = new TextField(12);
		txt2 = new TextField(12);

		add(txt1);
		add(txt2);

	}


	public void start()
	{
		if (count1 == null)
		{
			count1 = new Counter3(txt1);
			count1.start();
		}

		if (count2 == null)
		{
			count2 = new Counter3(txt2);
			count2.start();
		}
	}


	public void stop()
	{
		count1.stop();
		count1 = null;

		count2.stop();
		count2 = null;
	}


	public boolean mouseDown(Event e, int x, int y)
	{
		if (suspended)
		{
			count1.resume();
			count2.resume();
		}
		else
		{
			count1.suspend();
			count2.suspend();
		}
		suspended = !suspended;
		return true;
	}

}
