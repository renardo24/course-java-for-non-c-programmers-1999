import java.util.*;

// Uses inner (anonymous) class, so requires Java 1.1 compiler

public class SimpleStack3
{
	private Object[] items;
	private int top = 0;

	SimpleStack3(int maxCapacity)
	{
		items = new Object[maxCapacity];
	}
	
	public synchronized void push(Object item)
	{
		items[top++] = item;
	}
	
	public boolean isEmpty()
	{
		return top == 0;
	}
	
	// other stack methods go here...

	
	public Enumeration elements()
	{
		// Define Enumerator as a anonymous class

		//return this.new Enumerator();
		return new Enumeration() {
			// No constructors allowed
			int count = top;

			public boolean hasMoreElements()
			{
				return count > 0;
			}
			
			public Object nextElement()
			{
				synchronized (SimpleStack3.this)
				{
					if (count == 0)
						throw new NoSuchElementException("SimpleStack");
					return items[--count];
				}
			}
		};  // Note: this colon terminates the return statement

	}

	public static void main(String[] args)
	{
		SimpleStack3 s = new SimpleStack3(10);
		//Enumerator e = s.new Enumerator();
	}

}
