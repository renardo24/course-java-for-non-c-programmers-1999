import java.awt.*;


// Implement thread by implementing Runnable interface

public class Counter2 implements Runnable
{
	private TextField txt;
	private int count;

	public Counter2(TextField txt)
	{
		this.txt = txt;
	}
		
	public void run()
	{
		while (true)
		{
			count++;
			txt.setText("Count = " + count);
			try
			{
				Thread.sleep(100);
			}
			catch (InterruptedException e)
			{}
		}
	}

}