public class TestConversionsAndCasts
{
	public static void main(String[] args)
	{
		byte b1 = 3;
		short s1 = 130;
		s1 = b1;
		//b1 = s1;
		b1 = (byte)s1;
		System.out.println("b1 = " + b1);
 	}

}
