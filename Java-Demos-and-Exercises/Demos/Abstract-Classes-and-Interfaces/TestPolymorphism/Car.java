public abstract class Car
{
	private int numDoors;
	protected Engine engine;
	protected int gear; 

	public Car(int d, Engine e)
	{
		numDoors = d;
		engine = e;
	}


	public boolean start()
	{
		return engine.start();
	}


	public abstract int getSpeed();
	public abstract void selectGear(int g);

}
