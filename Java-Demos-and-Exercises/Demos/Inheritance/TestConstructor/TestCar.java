public class TestCar
{
	public static void main(String[] args)
	{
		Engine engine = new Engine(1400);
		Brava ba = new Brava(4, engine);
		
		engine = new Engine(2000);
		TurboUnit turbo = new TurboUnit();
		Bravissimo bs = new Bravissimo(2, engine, turbo);

		System.out.println("\nba refers to a " + ba);
		System.out.println("\nbs refers to a " + bs);
	}

}


