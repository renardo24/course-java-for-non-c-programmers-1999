import java.applet.*;
import java.awt.*;

// Single container with border layout

public class BorderLayoutTest extends Applet
{

	public void init()
	{
		setBackground(Color.white);

		// Change layout to border layout

		setLayout(new BorderLayout());

		add("North", new Button("Red"));
		add("East", new Button("Green"));
		add("South", new Button("Blue"));
		add("West", new Button("White"));
		//add("Center", new Button("Black"));
	}


	public boolean action(Event evt, Object arg)
	{
		boolean result = false;
		if (evt.target instanceof Button)
		{
			changeColor((String)arg);
			result = true;
		}
		return result;
	}

	private void changeColor(String s)
	{
		if (s.equals("Red")) setBackground(Color.red);
		else if (s.equals("Green")) setBackground(Color.green);
		else if (s.equals("Blue")) setBackground(Color.blue);
		else if (s.equals("White")) setBackground(Color.white);
		else if (s.equals("Black")) setBackground(Color.black);
		repaint();
	}

}
