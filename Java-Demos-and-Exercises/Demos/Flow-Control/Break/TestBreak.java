public class TestBreak
{

	public static void main(String[] args)
	{
		int age = 45;
		int balance = 50000;
		int payment = 3000;
		float interest = 0.10F;

		while (age <= 65)
		{
			balance = (int)((balance + payment) * (1 + interest));
			if (balance > 250000)
				break;
			age++;
		}
		if (age < 65)
			System.out.println("You can retire at " + age);
		System.out.println("The value of your pension fund will be " + balance);
	}

}
