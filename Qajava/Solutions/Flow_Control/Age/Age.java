/* Using if statements and the conditional operator */

public class Age
{
	public static void main(String argv[])
	{
        short myAge = 36, yourAge = 32;
        char mySex = 'M', yourSex = 'F';

        System.out.println("My age is " + myAge);
        System.out.println("Your age is " + yourAge);

		// Some interesting facts about us

        if (myAge >= 30 && yourAge >= 30)
            System.out.println("We are both over 30");

        if (myAge < 25 || yourAge < 25)
            System.out.println("One of us is under 25");

        if (myAge >= 40 && (mySex == 77 || mySex == 109)
				|| yourAge >= 40 && (yourSex == 77 || yourSex == 109))
            System.out.println("One of us is male and over 40");


		// Finally, another really interesting fact about us

		// First attempt using if statement

/*
		short maxAge;
		if (myAge > yourAge)
			maxAge = myAge;
		else
			maxAge = yourAge;
*/

		// Second attempt using conditional operator

        short maxAge = (myAge > yourAge) ? myAge : yourAge;
        System.out.println("Our maximum age is " + maxAge);

    }

}

