import java.io.*;
import java.net.*;


class SimpleClient
{
	public static void main(String[] args)
	{
		String host = "www.bath.ac.uk"; // University of Bath
		int port = 13; // time of day

		if (args.length == 2)
		{
			host = args[0];
			port = Integer.parseInt(args[1]);
		}

		try
        {
			Socket sock = new Socket(host, port);
            DataInputStream dis = new DataInputStream(sock.getInputStream());

			boolean more = true;
			while (more)
			{
				String line = dis.readLine();
				if (line == null)
					more = false;
				else
					System.out.println(line);
			}
		} 
		catch(IOException e)
		{
			System.out.println("Exception: " + e);
		}
	}

}
