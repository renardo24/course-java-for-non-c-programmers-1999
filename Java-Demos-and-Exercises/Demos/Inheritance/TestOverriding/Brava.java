public class Brava
{
	private String model = "Brava";
	private int numDoors;
	private Engine engine;
	private int gear; 

	public Brava(int d, Engine e)
	{
		numDoors = d;
		engine = e;
	}


	public boolean start()
	{
		return engine.start();
	}


	public void selectGear(int g)
	{
		gear = g;
	}


	public int getSpeed()
	{
		return (engine.getRevs()/100) * gear;
	}


	public String toString()
	{
	  return model + " " + numDoors + " door";
	}

}
