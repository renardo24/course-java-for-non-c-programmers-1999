public class StringBufferTest
{
	public String reverseIt(String s)
	{
		StringBuffer sb = new StringBuffer();
		for (int i = s.length() - 1; i >= 0; i--)
		{
			sb.append(s.charAt(i));
		}
		return sb.toString();
	}

	public static void main(String[] args)
	{
		StringBufferTest sm = new StringBufferTest();
		String rs = sm.reverseIt("This is a test\n");
		System.out.println(rs);
	}

}