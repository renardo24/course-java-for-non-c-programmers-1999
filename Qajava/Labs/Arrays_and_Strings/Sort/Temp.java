	// The sort() method implements a simple 'bubble sort' algorithm to
	// sort an array of integers into ascending order
	//
	public static void bSort(int[] nums)
	{
		for (int i = 0; i < nums.length; i++)
		{
			for (int j = i + 1; j < nums.length; j++)
			{
				if (nums[i] > nums[j])
				{
					// swap order
					int temp = nums[j];
					nums[j] = nums[i];
					nums[i] = temp;
				}
			}
		}

	}
