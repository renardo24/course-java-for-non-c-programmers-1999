public class Brava
{
	private String model = "Brava";
	protected int numDoors;
	private Engine engine;

	public Brava(int d, Engine e)
	{
		numDoors = d;
		engine = e;
	}

	public String toString()
	{
	  return model + " " + numDoors + " door";
	}

}
