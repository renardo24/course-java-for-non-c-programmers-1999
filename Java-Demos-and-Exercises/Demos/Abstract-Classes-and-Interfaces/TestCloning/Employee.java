public class Employee// implements Cloneable
{
	private StringBuffer name;
	private int idNumber;
	private static int nextIdNumber = 1;


	public Employee(String name)
	{
		synchronized(this.getClass())
		{
			idNumber = nextIdNumber++;
		}
		this.name = new StringBuffer(name);
	}


	public void insertFirstName(String firstName)
	{
		name.insert(0, firstName + " ");
	}


	public String toString()
	{
		return getClass().getName() + ": " + name + " (ID = " + idNumber + ")";
	}


	/*
	public Object clone() throws CloneNotSupportedException
	{
		Employee e = (Employee)(super.clone());

		synchronized(this.getClass())
		{
			e.idNumber = nextIdNumber++;
		}
		//e.name = new StringBuffer(e.name.toString());
		return e;
	}
	*/

}