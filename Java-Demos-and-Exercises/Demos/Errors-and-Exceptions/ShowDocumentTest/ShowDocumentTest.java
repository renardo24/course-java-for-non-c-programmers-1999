import java.applet.*;
import java.awt.*;
import java.net.*;

// Show another HTML page in another browser window.
//
// Demonstrates use of URL class and need to catch
// potential MalformedURLException

public class ShowDocumentTest extends Applet
{
	Button showQA;

	public void init()
	{
		add(showQA = new Button("Show QA Site"));
	}


	public boolean action(Event evt, Object arg)
	{
		boolean result = false;
		if (evt.target == showQA)
		{
			try
			{
				AppletContext context = getAppletContext();
				URL url = new URL("htp://www.qatraining.com");
				context.showDocument(url, "_blank"); // show in new window
			} catch (MalformedURLException e)
			{
				showStatus("Exception " + e);
			}
			result = true;
		}
		return result;
	}


}
