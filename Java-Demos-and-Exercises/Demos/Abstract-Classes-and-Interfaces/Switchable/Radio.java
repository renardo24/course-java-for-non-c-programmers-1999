import java.applet.*;
import java.awt.*;

class Radio extends Canvas implements Switchable
{
	boolean on = false;
	AudioClip audioClip;

	public Radio(AudioClip ac)
	{
		audioClip = ac;
	}
	
	
	public void on()
	{
		on = true;
		repaint();
		audioClip.loop();
	}
	
	public void off()
	{
		on = false;
		repaint();
		audioClip.stop();
	}


	public void paint(Graphics g)
	{
		g.setColor(Color.darkGray);
		g.fillRect(50, 50, 100, 100);
		if (on)
			g.setColor(Color.lightGray);
		else
			g.setColor(Color.gray);
		g.fillOval(60, 60, 80, 80);
		g.setColor(Color.darkGray);
		g.fillRect(60, 100, 80, 50);
		g.fillOval(95, 95, 10, 10);
		g.drawOval(70, 70, 60, 60); 
		g.drawOval(80, 80, 40, 40); 
		g.drawLine(100, 100, 130, 60);
		g.setColor(Color.gray);
		g.fillOval(70, 120, 10, 10);
		g.fillOval(120, 120, 10, 10);
	}



}
