import java.net.*;

// This DOES compile!

public class MyClass
{

	public URL changeURL(URL oldURL)
	{
		try
		{
			URL u = new URL("http://www.qatraining.com");
			return u;
		}
		catch (MalformedURLException e)
		{
			return oldURL;
		}
	}

}
