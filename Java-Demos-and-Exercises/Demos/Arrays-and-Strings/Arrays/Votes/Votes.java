public class Votes
{
	public static void main(String[] args)
	{
		// Declare a variable capable of holding a reference to an array of ints
		int[] votes;

		// Create an array of 3 ints
		votes = new int[3];

		// Allocate random value to each element in the array
		for (int i = 0; i < votes.length; i++)
			votes[i] = (int)(10000 * Math.random());

		// Print contents of array
		System.out.println("\nThe results of the election were:\n");
		for (int i = 0; i < votes.length; i++)
			System.out.println("  " + (i+1) + ".  " + votes[i] + "\n");
	}

}
