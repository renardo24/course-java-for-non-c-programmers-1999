public class CarTest
{
	public static void main(String[] args)
	{
		Car car1, car2;

		car1 = new Car();
		car1.model = "Ford";
		car2 = new Car();

		car2.model = "BMW";
		System.out.println("Car 1 is a " +  car1.model         );
		System.out.println("Car 2 is a " +  car2.model         );
	}
}


