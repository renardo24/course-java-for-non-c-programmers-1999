import java.applet.*;
import java.awt.*;
import java.awt.event.*;

// Now uses inner class, so no need for applet class to
// implement ActionListener interface.

public class ButtonClickWithInnerClass extends Applet
{
	TextField text;
	Button button;

	public void init()
	{
		button = new Button("Click me!");

		// use anonymous inner class
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				text.setText("You clicked the button!");
			}
		});

		add(button);
	
		text = new TextField(20);
		add(text);
	}

}
