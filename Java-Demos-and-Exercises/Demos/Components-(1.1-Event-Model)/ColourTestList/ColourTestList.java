import java.applet.*;
import java.awt.*;
import java.awt.event.*;

// Using a List component

public class ColourTestList extends Applet implements ItemListener
{
	private List colourList;

	public void init()
	{
		add(new Label("Background colour:"));

		add(colourList = new List(5, false)); // 5 rows, single selection
		colourList.addItem("Red");
		colourList.addItem("Green");
		colourList.addItem("Blue");
		colourList.addItem("White");
		colourList.addItem("Black");

		setBackground(Color.white);
		colourList.select(3);  // select white as initial colour

		colourList.addItemListener(this);
	}


	public void itemStateChanged(ItemEvent evt)
	{
		if (evt.getSource() == colourList && evt.getStateChange() == ItemEvent.SELECTED)
		{
			String value = colourList.getSelectedItem();
			changeColor(value);
		}
	}


	private void changeColor(String s)
	{
		if (s.equals("Red")) setBackground(Color.red);
		else if (s.equals("Green")) setBackground(Color.green);
		else if (s.equals("Blue")) setBackground(Color.blue);
		else if (s.equals("White")) setBackground(Color.white);
		else if (s.equals("Black")) setBackground(Color.black);
		repaint();
	}

}

