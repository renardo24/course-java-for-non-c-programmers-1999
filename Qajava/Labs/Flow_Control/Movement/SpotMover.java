import java.applet.*;
import java.awt.*;

public class SpotMover extends Applet implements Runnable {

int screenWidth, screenHeight;      // the Applet's dimensions (do not alter)
int spotDiameter = 20;              // the diameter of the spot

int x = 0;                          // the x-coordinate for the spot
int y = 0;                          // the y-co-ordinate for the spot


// The following run() method alters the values of the variables x and y
// to alter the position of a spot on the applet's screen.
public void run()
{
        //
        // ToDo:
        //
        // Replace the following code with control structures to change the values of
        // the x and y variables so that the spot takes a different path around the screen
        // Call the paintAndPause() method to refresh the screen display.
    
        for(int i = 0; i < (screenWidth -x);i++)
        {
            x += 2;
            y += 1;
            paintAndPause(20);          // repaint screen and pause for 20 milliseconds
        }

        paintAndPause(100);             // repaint screen and pause for 50 milliseconds

        y +=  (spotDiameter * 2);

        paintAndPause(0);               // repaint screen and pause for 10 milliseconds


}   // end of run() method





/* ******* You do not need to change the code below this point. ***** */



public void paintAndPause(int delay){

       repaint();
       try{
            Thread.sleep(delay);
       }catch(InterruptedException IE){}
}




public  void paint(Graphics g)
{
       g.setColor(Color.cyan);
       g.fillOval(x,y,spotDiameter,spotDiameter);
}


public void init()
{
   setBackground(Color.black);
   setForeground(Color.white);
   screenWidth = size().width;      // Java 1.1 uses getSize()
   screenHeight = size().height;

}

public void start()
{
    if(thread == null){
        thread = new Thread(this);
        thread.start();
    }
}
public void stop()
{
    if(thread != null){
        thread.stop();
        thread = null;
    }
}

private Thread thread;


}