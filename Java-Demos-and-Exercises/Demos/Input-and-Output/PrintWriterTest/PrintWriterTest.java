import java.io.*;

// This version is designed for Java 1.1.  Because DataInputStream is deprecated
// it uses the BufferReader class, which provides a readLine() method. An
// InputStreamReader is used to convert the byte stream from System.in into a 
// character stream. A PrintWriter is used instead of the deprecated PrintStream class.

public class PrintWriterTest
{

	public static void main(String[] args) throws IOException
	{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringBuffer sb = new StringBuffer(128);
		String s;

		if (args.length == 1)
			out = new PrintWriter(new FileWriter(args[0]));

		byte buffer[] = new byte[128];

		// read in a line of text
		while (true)
		{
			s = in.readLine();
			if (s != null)
				sb.append(s + '\n');
			else
				break;
		}
		
		if (sb.length() != 0)
		{
			s = sb.toString();
			out.println(s);
		}
		out.close();
	}

}
