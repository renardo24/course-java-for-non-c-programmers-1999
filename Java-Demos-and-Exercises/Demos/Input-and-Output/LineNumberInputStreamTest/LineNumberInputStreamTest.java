import java.io.*;

public class LineNumberInputStreamTest
{

	public static void main(String[] args) throws IOException
	{
		if (args.length != 2)
		{
			System.out.println("Usage: LineNumberInputStreamTest "  +
									"<file name> <character>");
			System.exit(0);
		}

		FileInputStream inFile = new FileInputStream(args[0]);
		LineNumberInputStream in = new LineNumberInputStream(inFile);
		int searchValue = args[1].charAt(0);

		int ch;		
		while ((ch = in.read()) != -1)
		{
			if (ch == searchValue)
			{
				System.out.println("Found character " + (char)searchValue +
									" at line " + (in.getLineNumber() +1));
				break;
			}
		}
		in.close();
	}

}

