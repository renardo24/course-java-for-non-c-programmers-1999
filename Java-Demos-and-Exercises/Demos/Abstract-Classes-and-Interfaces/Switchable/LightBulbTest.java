import java.applet.*;
import java.awt.*;

public class LightBulbTest extends Applet
{
	SwitchPanel switchPanel;
	LightBulb lightBulb;

    public void init()
    {
		setLayout(new BorderLayout());

		lightBulb = new LightBulb();
		switchPanel = new SwitchPanel(lightBulb);

		add("South", switchPanel);
		add("Center", lightBulb);
	}

}

