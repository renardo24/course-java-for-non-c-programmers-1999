import java.util.*;

// Uses inner class, so requires Java 1.1 compiler

public class SimpleStack
{
	private Object[] items;
	private int top = 0;

	SimpleStack(int maxCapacity)
	{
		items = new Object[maxCapacity];
	}
	
	public synchronized void push(Object item)
	{
		items[top++] = item;
	}
	
	public boolean isEmpty()
	{
		return top == 0;
	}
	
	// other stack methods go here...

	
	/** This adapter class is defined as part of its target class,
	 *  It is placed alongside the variables it needs to access.
	 */
	
	private class Enumerator implements Enumeration
	{
		int count;

		private Enumerator()
		{
			//this.count = SimpleStack.this.top;
			count = top;
		}
		
		public boolean hasMoreElements()
		{
			return count > 0;
		}
		
		public Object nextElement()
		{
			synchronized (SimpleStack.this)
			{
				if (count == 0)
					throw new NoSuchElementException("SimpleStack");
				return items[--count];
			}
		}
	}
	
	public Enumeration elements()
	{
		//return this.new Enumerator();
		return new Enumerator();
	}

	public static void main(String[] args)
	{
		SimpleStack s = new SimpleStack(10);
		//Enumerator e = s.new Enumerator();
	}

}
