import java.applet.*;
import java.awt.*;

public class ButtonClickTest extends Applet
{
	TextField text;
	Button button;

	public void init()
	{
		button = new Button("Click me!");
		add(button);
	
		text = new TextField(20);
		add(text);
	}

	public boolean action(Event e, Object arg)
	{
		if (e.target == button)
		{
			text.setText("You clicked the button!");
			return true;
		}
		else
			return false;
	}

}
