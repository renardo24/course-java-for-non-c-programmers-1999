public class TestCar
{
	public static void main(String[] args)
	{
		Car car1 = new Car();
		car1.model = "BMW";

		Car car2 = new Car();
		car2.model = "Audi";

		System.out.println("\ncar1 is a " + car1.model);
		System.out.println("car2 is a " + car2.model);

		car1.speed = 60;
		car2.speed = 50;

		System.out.println("\ncar1 is now doing " + car1.speed);
		System.out.println("car2 is now doing " + car2.speed);


	}

}
