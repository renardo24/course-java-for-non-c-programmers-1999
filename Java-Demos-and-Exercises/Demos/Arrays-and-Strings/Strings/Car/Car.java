public class Car
{
	private String model;
	private int numDoors = 4;

	public Car(String m, int d)
	{
		model = m;
		numDoors = d;
	}

	
	public String toString()
	{
		return numDoors + " door " + model;
	}
	

	public static void main(String[] args)
	{
		Car myCar = new Car("Cavalier", 5);
		System.out.println("\nMy car is a " + myCar);
	}

}