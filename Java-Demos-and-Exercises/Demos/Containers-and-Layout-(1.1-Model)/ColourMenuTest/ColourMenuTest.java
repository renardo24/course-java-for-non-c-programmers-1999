import java.applet.*;
import java.awt.*;
import java.awt.event.*;

public class ColourMenuTest extends Frame implements ActionListener
{

	public ColourMenuTest()
	{
		MenuItem m;

		setBackground(Color.white);

		MenuBar mb = new MenuBar();
		Menu colorMenu = new Menu("Color");

		colorMenu.add(m = new MenuItem("Red"));
		m.addActionListener(this);

		colorMenu.add(m = new MenuItem("Green"));
		m.addActionListener(this);

		colorMenu.add(m = new MenuItem("Blue"));
		m.addActionListener(this);

		colorMenu.add(m = new MenuItem("White"));
		m.addActionListener(this);

		colorMenu.add(m = new MenuItem("Black"));
		m.addActionListener(this);

		mb.add(colorMenu);
		setMenuBar(mb);

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt)
			{
				System.exit(0);
			}
		});

	}


	public void actionPerformed(ActionEvent evt)
	{
		changeColor(evt.getActionCommand());
	}


	private void changeColor(String s)
	{
		if (s.equals("Red")) setBackground(Color.red);
		else if (s.equals("Green")) setBackground(Color.green);
		else if (s.equals("Blue")) setBackground(Color.blue);
		else if (s.equals("White")) setBackground(Color.white);
		else if (s.equals("Black")) setBackground(Color.black);
		repaint();
	}


	public static void main(String[] args)
	{
		Frame f = new ColourMenuTest();
		f.setSize(200, 200);
		f.show();
	}


}
