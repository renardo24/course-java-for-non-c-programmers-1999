import java.io.*;

public class FileOutputStreamTest
{

	public static void main(String[] args) throws IOException
	{
		InputStream in = System.in;
		OutputStream out = System.out;

		if (args.length == 1)
			out = new FileOutputStream(args[0]);

		int ch;

		while ((ch = in.read()) != -1)
		{
			if (Character.isUpperCase((char)ch))
				ch = Character.toLowerCase((char)ch);
			out.write(ch);
		}

	}

}


