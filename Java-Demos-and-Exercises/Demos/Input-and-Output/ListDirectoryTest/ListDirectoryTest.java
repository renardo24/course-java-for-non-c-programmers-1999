import java.io.*;

public class ListDirectoryTest
{

	public static void main(String[] args)
	{
		String fileName;

		if (args.length == 1)
			fileName = args[0];
		else
			fileName = "d:\\temp";


		File f = new File(fileName);

		if (f.isDirectory())
		{
			System.out.println("Directory of " + fileName + ":");
			String[] files = f.list();
			for (int i = 0; i < files.length; i++)
				System.out.println(files[i]);
		}
		else
			System.out.println("Error: " + args[0] + " is not a directory");
	}

}
