import java.awt.*;
import java.awt.event.*;
import com.sun.java.swing.*;
import com.sun.java.swing.preview.*;   // not required with Swing 1.1


public class ColorChooserTest extends JFrame
{
    public ColorChooserTest(String title)
    {
        super(title);
        final Container cp = getContentPane();

		JMenuBar mb = new JMenuBar();
		JMenu colorMenu = new JMenu("Options");

		JMenuItem mi = colorMenu.add(new JMenuItem("Color..."));
		mi.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent evt)
		    {
		        Color color = JColorChooser.showDialog(ColorChooserTest.this,
                                                       "Choose a Background Color",
                                                       Color.lightGray);
                cp.setBackground(color);
				cp.repaint();
            }
        });

		mb.add(colorMenu);
		setJMenuBar(mb);
	}


	public static void main(String[] args)
	{
		JFrame f = new ColorChooserTest("Color Chooser Test");
        f.setSize(300, 200);
        f.setLocation(200, 300);
        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e)
            {
                System.exit(0);
            }
        });
        f.setVisible(true);
    }
}