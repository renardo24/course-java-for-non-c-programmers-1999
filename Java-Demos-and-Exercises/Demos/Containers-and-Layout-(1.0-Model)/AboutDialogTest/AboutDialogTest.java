import java.awt.*;

public class AboutDialogTest extends Frame
{

	public AboutDialogTest()
	{
		setLayout(new FlowLayout());
		add(new Button("About"));
	}

	public boolean action(Event evt, Object arg)
	{
		Dialog dialog = null;
		if (arg.equals("About"))
		{
			if (dialog == null)
				dialog = new AboutDialog(this);
			return true;
		}
		return false;
	}


	public boolean handleEvent(Event evt)
	{
		if (evt.id == Event.WINDOW_DESTROY)
			System.exit(0);
		return super.handleEvent(evt);
	}


	public static void main(String[] args)
	{
		Frame f = new AboutDialogTest();
		f.resize(300, 200);
		f.show();
	}


}

