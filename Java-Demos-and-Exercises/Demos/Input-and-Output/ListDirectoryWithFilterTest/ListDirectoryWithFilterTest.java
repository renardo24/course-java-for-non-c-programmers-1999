import java.io.*;

public class ListDirectoryWithFilterTest implements FilenameFilter 
{

	public boolean accept(File dir, String name)
	{
		boolean result = false;
		if (name.endsWith("html"))
			result = true;
		return result;
	}


	public static void main(String[] args)
	{
		String fileName;

		ListDirectoryWithFilterTest ld = new ListDirectoryWithFilterTest();
		
		if (args.length == 1)
			fileName = args[0];
		else
			fileName = "d:\\temp";

		File f = new File(fileName);
		if (f.isDirectory())
		{
			System.out.println("Directory of " + fileName + ":\n");
			String[] files = f.list(ld);
			for (int i = 0; i < files.length; i++)
				System.out.println(files[i]);
		}
		else
			System.out.println("Error: " + args[0] + " is not a directory");

	}

}
