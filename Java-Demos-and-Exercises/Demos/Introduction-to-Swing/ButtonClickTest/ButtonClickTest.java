import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import com.sun.java.swing.*;


public class ButtonClickTest extends JApplet implements ActionListener
{
	JTextField text;
	JButton button;

	public void init()
	{
	    Container cp = getContentPane();
	    cp.setLayout(new FlowLayout());
	    
	    button = new JButton("Click me!");
		button.addActionListener(this);
		cp.add(button);
	
		text = new JTextField(20);
		cp.add(text);
	}

	public void actionPerformed(ActionEvent e)
	{
		text.setText("You clicked the button!");
	}

}
