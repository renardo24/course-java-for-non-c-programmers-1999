import java.util.*;

public class VectorCapacityTest
{

	public static void main(String[] args)
	{
		// create a vector with a capacity of 3
		Vector items = new Vector(3);


		// check size and capacity of vector
		System.out.println("Capacity = " + items.capacity()
								+ ", Size = " + items.size());

		// initialise vector with references to 3 items
		items.addElement(new Item("Bread", 1));
		items.addElement(new Item("Milk", 3));
		items.addElement(new Item("Coffee", 1));

		// re=check size and capacity of vector
		System.out.println("Capacity = " + items.capacity()
								+ ", Size = " + items.size());
		 
		// add a fourth Item
		items.addElement(new Item("Sugar", 2));

		// re=check size and capacity of vector
		System.out.println("Capacity = " + items.capacity()
								+ ", Size = " + items.size());
	}

}