public class Bravissimo extends Brava
{
	private String model = "Bravissimo";
	private TurboUnit turbo;

	public Bravissimo (int d, Engine e, TurboUnit t)
	{
		super(d, e);
		turbo = t;
	}

	public String toString()
	{
	  return model + " " + numDoors + " door";
	}

}

