public class CarSwap
{
	public static void swapCars(Car c1, Car c2)
	{
		Car temp = c2;
		c2 = c1;
		c1 = temp;
	}

	public static void main(String[] args)
	{
		Car car1, car2;

		System.out.println("\nCreating two cars ...\n");
		car1 = new Car("Ford");
		car2 = new Car("BMW");

		System.out.println("Car 1 is a " + car1.model);
		System.out.println("Car 2 is a " + car2.model);

		System.out.println("\nSwapping cars ...\n");
		swapCars(car1, car2);

		System.out.println("Car 1 is a " + car1.model);
		System.out.println("Car 2 is a " + car2.model);
	}
}


