public class Car
{

	private int speed;
	private byte currentGear;
	private byte numGears = 5;


	public boolean selectGear(byte newGear)
	{
		boolean result = false;
		if (newGear >= 0 && newGear <= numGears)
		{
			currentGear = newGear;
			result = true;
		}
		return result;
	}


	public void accelerate()
	{
		speed += 5;
	}

	
	public int getSpeed()
	{
		return speed;
	}

}
