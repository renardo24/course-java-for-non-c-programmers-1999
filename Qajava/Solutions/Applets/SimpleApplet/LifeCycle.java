<html>
<head>
<title>LifeCycle applet</title>
</head>
<body>
<h1>LifeCycle Applet</h1>
<hr>
This is a sample page into which you should embed your Java applet.
Put the applet just after this paragraph.<p>

<applet code=LifeCycle width=300 height=100>
</applet>
<p>
<a href=LifeCycle.java>The code.</a>
</body>
</html>