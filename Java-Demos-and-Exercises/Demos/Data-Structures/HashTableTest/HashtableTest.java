import java.util.*;

public class HashtableTest
{

	public static void main(String[] args)
	{
		// create an empty hashtable
		Hashtable prices = new Hashtable();

		// initialise hashtable with 3 prices
		// Note: must use wrapper class
		prices.put("Bread", new Double(0.85));
		prices.put("Milk", new Double(0.38));
		prices.put("Coffee", new Double(1.50));


		// change price of milk
		prices.put("Milk", new Double(0.36));

		System.out.println();

		// enumerate prices held in hashtable
		Enumeration e1 = prices.elements();
		while (e1.hasMoreElements())
		{
			double price = ((Double)e1.nextElement()).doubleValue();  // note cast
			System.out.println(price);
		}

		System.out.println();

		// enumerate keys and prices held in hashtable
		Enumeration e2 = prices.keys();
		while (e2.hasMoreElements())
		{
			// get next key
			String key = (String)e2.nextElement();  // note cast
			// now get its price
			Double price = (Double)prices.get(key);
			System.out.println(key + "\t" + price);
		}


	}

}