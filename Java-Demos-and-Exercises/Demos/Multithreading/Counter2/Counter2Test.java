import java.applet.*;
import java.awt.*;

// Creates two counter threads

public class Counter2Test extends Applet
{
	private TextField txt1, txt2;
	private Counter2 count1, count2;
	Thread thread1, thread2;
	boolean suspended;

	public void init()
	{
		txt1 = new TextField(12);
		txt2 = new TextField(12);

		add(txt1);
		add(txt2);

	}


	public void start()
	{
		if (thread1 == null)
		{
			count1 = new Counter2(txt1);
			thread1 = new Thread(count1);
			thread1.start();
		}

		if (thread2 == null)
		{
			count2 = new Counter2(txt2);
			thread2 = new Thread(count2);
			thread2.start();
		}
	}


	public void stop()
	{
		thread1.stop();
		thread1 = null;
		thread2.stop();
		thread2 = null;
	}


	public boolean mouseDown(Event e, int x, int y)
	{
		if (suspended)
		{
			thread1.resume();
			thread2.resume();
		}
		else
		{
			thread1.suspend();
			thread2.suspend();
		}
		suspended = !suspended;
		return true;
	}

}
