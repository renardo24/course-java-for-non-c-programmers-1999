import java.applet.*;
import java.awt.*;
import java.util.*;
import java.io.*;
import java.net.*;

public class ShowTextFile extends Applet
{
	private Font font;
	private DataInputStream din;

	public void init()
	{
		try
		{
			URL url = new URL(getDocumentBase(), "drivers.txt");
			InputStream in = url.openStream();
			din = new DataInputStream(new BufferedInputStream(in));
		}
		catch (Exception e)
		{
			showStatus("Exception " + e);
		}

	}


	public void paint(Graphics g)
	{
		font = new Font("Courier", Font.BOLD, 16);
		g.setFont(font);
		FontMetrics fm = g.getFontMetrics(font);
		int height = fm.getHeight();
		int x = 0;
		int y = height;
		String text;
		try
		{
			while ((text = din.readLine()) != null)
				g.drawString(text, 0, y += height);
		} catch (IOException e)
		{
			showStatus("Exception " + e);
		}

	}


}

