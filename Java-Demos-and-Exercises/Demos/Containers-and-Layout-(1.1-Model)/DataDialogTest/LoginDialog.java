import java.awt.*;
import java.awt.event.*;

public class LoginDialog extends Dialog
{
	Frame parent;
	TextField nameField, pswdField;
	String name, password;

	public LoginDialog(Frame parent)
	{
		super((Frame)parent, "Login", true);  // modal
		this.parent = parent;
		setLayout(new FlowLayout());
		add(new Label("Name"));
		add(nameField = new TextField(12));
		add(new Label("Password"));
		add(pswdField = new TextField(12));
		pswdField.setEchoChar('*');

		Button ok, cancel;
		add(ok = new Button("OK"));
		add(cancel = new Button("Cancel"));
		pack();

		ok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt)
			{
				name = nameField.getText();
				password = pswdField.getText();
				dispose();
			}
		});

		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt)
			{
				dispose();
			}
		});

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we)
			{
				dispose();
			}
		});
	}

	public String getName()
	{
		return name;
	}

	public String getPassword()
	{
		return password;
	}


}
