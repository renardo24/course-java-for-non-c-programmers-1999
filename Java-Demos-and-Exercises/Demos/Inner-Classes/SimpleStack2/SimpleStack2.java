import java.util.*;

// Uses inner (local) class, so requires Java 1.1 compiler

public class SimpleStack2
{
	private Object[] items;
	private int top = 0;

	SimpleStack2(int maxCapacity)
	{
		items = new Object[maxCapacity];
	}
	
	public synchronized void push(Object item)
	{
		items[top++] = item;
	}
	
	public boolean isEmpty()
	{
		return top == 0;
	}
	
	// other stack methods go here...

	
	public Enumeration elements()
	{
		// Define Enumerator as a local class
		class Enumerator implements Enumeration
		{
			int count;

			private Enumerator()
			{
				//this.count = SimpleStack.this.top;
				count = top;
			}
			
			public boolean hasMoreElements()
			{
				return count > 0;
			}
			
			public Object nextElement()
			{
				synchronized (SimpleStack2.this)
				{
					if (count == 0)
						throw new NoSuchElementException("SimpleStack");
					return items[--count];
				}
			}
		}

		//return this.new Enumerator();
		return new Enumerator();
	}

	public static void main(String[] args)
	{
		SimpleStack2 s = new SimpleStack2(10);
		//Enumerator e = s.new Enumerator();
	}

}
