import java.util.*;

public class VectorAccessTest
{

	public static void main(String[] args)
	{
		// create an empty vector
		Vector items = new Vector();

		// initialise vector with references to 3 items
		items.addElement(new Item("Bread", 1));
		items.addElement(new Item("Milk", 3));
		items.addElement(new Item("Coffee", 2));


		// change one of the entries
		items.setElementAt(new Item("Milk", 4), 1);


		// list contents of vector
		for (int i = 0; i < items.size(); i++)
		{
			Item item = (Item)items.elementAt(i);  // note cast
			System.out.println(item.toString());
		}

	}

}