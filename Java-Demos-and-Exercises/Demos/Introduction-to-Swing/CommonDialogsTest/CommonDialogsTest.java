import java.util.*;
import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import com.sun.java.swing.*;


public class CommonDialogsTest extends JFrame implements ActionListener
{
    private Container cp;
    private static Hashtable colorTable;
    private static final String[] colorNames = {"Red", "Green", "Blue"};
    private static final Color[] colors = {Color.red, Color.green, Color.blue};
    private int colorIndex = 0;

    static
    {
        colorTable = new Hashtable();
        for (int i = 0; i < colorNames.length; i++)
            colorTable.put(colorNames[i], colors[i]);
    }

	public CommonDialogsTest()
	{
		JMenuItem mi;

        cp = getContentPane();

		JMenuBar mb = new JMenuBar();
		JMenu colorMenu = new JMenu("Options");

		colorMenu.add(mi = new JMenuItem("Message..."));
		mi.setActionCommand("message");
		mi.addActionListener(this);
		colorMenu.add(mi = new JMenuItem("Confirm..."));
		mi.setActionCommand("confirm");
		mi.addActionListener(this);
		colorMenu.add(mi = new JMenuItem("Option..."));
		mi.setActionCommand("option");
		mi.addActionListener(this);
		colorMenu.add(mi = new JMenuItem("Input..."));
		mi.setActionCommand("input");
		mi.addActionListener(this);

		mb.add(colorMenu);
		setJMenuBar(mb);


	}


	public void actionPerformed(ActionEvent evt)
	{
		String command = evt.getActionCommand();
		if (command.equals("message"))
		{
		    JOptionPane.showMessageDialog(this,
		                                  "Press OK to change background colour"
		                                  );
            changeColor();
        }
        else if (command.equals("confirm"))
		{
		    int response = JOptionPane.showConfirmDialog(this,
		                                  "Do you want to change the background colour?"
		                                  );
            switch (response)
            {
                case JOptionPane.YES_OPTION:
                    changeColor();
                case JOptionPane.NO_OPTION:
                case JOptionPane.CANCEL_OPTION:
                case JOptionPane.CLOSED_OPTION:
                    // do nothing
            }
        }
		else if (command.equals("option"))
		{
		    int option = JOptionPane.showOptionDialog(this,
		                                  "Please select background color",
		                                  "Background Colour Selection",
		                                  JOptionPane.DEFAULT_OPTION, // not used
		                                  JOptionPane.QUESTION_MESSAGE,
		                                  null,  // no custom icon
		                                  colorNames,
		                                  colorNames[0]  // default button
		                                  );

            if (option != JOptionPane.CLOSED_OPTION && option <= colors.length)
            {
                changeColor(option);
            }
        }
		else if (command.equals("input"))
		{
		    String string = (String)JOptionPane.showInputDialog(this,
		                                  "Please select background color",
		                                  "Background Colour Selection",
		                                  JOptionPane.QUESTION_MESSAGE,
		                                  null,  // no custom icon
		                                  colorNames,
		                                  colorNames[0]  // default button
		                                  );

            if (string != null && !string.equals(""))
            {
                changeColor(string);
            }
        }

	}


    private void changeColor()
    {
        cp.setBackground(colors[colorIndex++]);
		cp.repaint();
        if (colorIndex == colors.length) colorIndex = 0;
    }

    private void changeColor(int index)
    {
        cp.setBackground(colors[index]);
		cp.repaint();
    }

    private void changeColor(String colorName)
    {
        Color color = (Color)colorTable.get(colorName);
        if (color != null) cp.setBackground(color);
		cp.repaint();
    }


	public static void main(String[] args)
	{
		JFrame f = new CommonDialogsTest();
		f.setSize(200, 200);
		f.setLocation(300, 200);
		f.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt)
			{
			    System.exit(0);
			}
		});
		f.setVisible(true);
	}


}
