public class TestCar
{
	public void speedTest(Car c)
	{
		c.getSpeed();
		System.out.println(c + " is doing " + c.getSpeed() + " mph");
	}


	public static void main(String[] args)
	{
		TestCar test = new TestCar();
		
		Engine engine = new Engine(1400);
		Brava ba1 = new Brava(4, engine);

		engine = new Engine(2000);
		TurboUnit turbo = new TurboUnit();

		Bravissimo bs1 = new Bravissimo(2, engine, turbo);
		bs1.setBoost((float)1.2);

		engine = new Engine(1400);
		Brava ba2 = new Brava(4, engine);

		Car[] cars = {ba1, bs1, ba2};
		
		for (int i = 0; i < cars.length; i++)
		{
			System.out.println("\nCar " + (i+1) + " is a " + cars[i].getClass().getName());

			cars[i].start();
			cars[i].selectGear(1);
			test.speedTest(cars[i]);
		}

	}

}


