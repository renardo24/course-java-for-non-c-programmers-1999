public class CreditAccount extends BankAccount
{
	private float limit;
	private static float interestRate;

	public CreditAccount(String name, float balance, float limit)
	{
		super(name, balance);
		this.limit = limit;
	}

	public CreditAccount(String name, float balance)
	{
		super(name, balance);
	}

	public CreditAccount(String name)
	{
		super(name, 0);
	}


	public boolean setLimit(float newLimit)
	{
		boolean result = false;

		if (newLimit >= 0)
		{
			limit = newLimit;
			result = true;
		}
		return result;
	}

	public float getLimit()
	{
		return limit;
	}


	// simplified version of addInterest() for demo purposes only

	public void addInterest()
	{
		float balance = getBalance();
		if (balance < 0)
			setBalance(balance * (1 + interestRate/100.0F));
	}


	public boolean withdraw(float amount)
	{
		if (getBalance() >= amount - limit)
		{
			setBalance(getBalance() - amount);
			return true;
		}
		else
			return false;
	}


	public static void setInterestRate(float rate)
	{
		interestRate = rate;
	}

	public static float getInterestRate()
	{
		return interestRate;
	}


}