import java.net.*;

public class PuttingItAllTogether
{
	String host = "www.qatraining.com";
	InetAddress addr;
	URL u;

	void makeConnection()
	{
		try
		{
			addr = InetAddress.getByName(host);
			u = new URL("http://" + host);
		}
		catch (UnknownHostException e)
		{
			System.out.println("Don't know host " + host  + ": " + e);
			return;
		}
		catch (MalformedURLException e)
		{
			System.out.println("Can't contact htp//" + host  + ": " + e);
			return;
		}

		finally
		{
			System.out.println("This will always be done");
		}
		System.out.println("Only see this if successful");
	}


	public static void main(String[] args)
	{
		PuttingItAllTogether p = new PuttingItAllTogether();
		p.makeConnection();
	}

}