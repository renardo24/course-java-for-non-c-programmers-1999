import java.applet.*;
import java.awt.*;
import java.awt.event.*;

public class TemperatureConverter extends Applet implements ActionListener
{
	private Button convert, reset;
	private TextField degC, degF;

	public void init()
	{
		add(new Label("Temperature Converter"));
		add(new Label("Temperature in deg C"));
		add(degC = new TextField(3));  // 3 columns max
		add(new Label("Temperature in deg F is "));
		add(degF = new TextField(3));
		add(convert = new Button("Convert"));
		add(reset = new Button("Reset"));
		degC.requestFocus();
		degF.setEditable(false);

		convert.addActionListener(this);
		reset.addActionListener(this);
		degC.addActionListener(this);
	}


	public void actionPerformed(ActionEvent evt)
	{
		Object source = evt.getSource();
		if (source == convert || source == degC)
		{
			int temp = Integer.parseInt(degC.getText());
			degF.setText("" + (temp * 9/5 + 32.0F));
		}
		else if (source == reset)
		{
			degF.setText("");     // clear text fields
			degC.setText("");
			degC.requestFocus();  // return focus to entry field
		}
	}


}
