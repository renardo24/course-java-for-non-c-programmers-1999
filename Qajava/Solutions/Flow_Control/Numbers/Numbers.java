/* Using loops */

public class Numbers
{
	public static void main(String argv[])
	{

		// Print out table of 2 to the power n where n < 64

		// First attempt using while loop
		/*
		int i = 1;
		long result = 1;
		while (i < 64)
		{
			result *= 2;
			if (result >= 10000000000)
				break;
			if (i < 10)
				System.out.println(i + "     " + result);
			else
				System.out.println(i + "    " + result);
			i++;
		}
		*/

		// Second attempt using a for loop
		/*
		long result = 1;
		for (int i = 1; i < 64; i++)
		{
			result *= 2;
			if (result >= 10000000000)
				break;
			if (i < 10)
				System.out.println(i + "     " + result);
			else
				System.out.println(i + "    " + result);
		}
		*/

		// Third (optional) attempt using a nested loop to
		// justify all numbers to the right

		long result = 1;
		for (int i = 1; i < 64; i++)
		{
			result *= 2;
			//if (result >= 10000000000)
			//	break;
			if (i < 10)
				System.out.print(" " + i);
			else
				System.out.print(i);
			//for (long j = 1; j < 10000000000; j *= 10)
			//{
			//	if (result < j)
			//		System.out.print(" ");
			//}
			System.out.println("   " + result);
		}

    }

}

