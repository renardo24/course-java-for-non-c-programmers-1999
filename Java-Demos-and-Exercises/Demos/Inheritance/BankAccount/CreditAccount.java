public class CreditAccount extends BankAccount
{
	private float limit;

	public CreditAccount(String name, float balance, float limit)
	{
		super(name, balance);
		this.limit = limit;
	}

	public CreditAccount(String name, float balance)
	{
		super(name, balance);
	}

	public CreditAccount(String name)
	{
		super(name, 0);
	}

	public boolean setLimit(float newLimit)
	{
		boolean result = false;

		if (newLimit >= 0)
		{
			limit = newLimit;
			result = true;
		}
		return result;
	}

	public float getLimit()
	{
		return limit;
	}

	public boolean withdraw(float amount)
	{
		if (getBalance() >= amount - limit)
		{
			setBalance(getBalance() - amount);
			return true;
		}
		else
			return false;
	}

	public static void main(String[] args)
	{
		CreditAccount account = new CreditAccount("Cripps", 100);

		account.deposit((float)1000.00);
		System.out.println("Balance for " + account.getName() +
			" is " + account.getBalance());

		account.withdraw(1000);
		System.out.println("Balance for " + account.getName() +
			" is " + account.getBalance());

		account.withdraw(500);
		System.out.println("Balance for " + account.getName() +
			" is " + account.getBalance());

	}

}