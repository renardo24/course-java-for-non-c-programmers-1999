public class Bravissimo extends Brava
{
	private TurboUnit turbo;

	public Bravissimo (int d, Engine e, TurboUnit t)
	{
		super(d, e);
		turbo = t;
	}

	public void setBoost(float b)
	{
		turbo.setBoost(b);
	}


	public int getSpeed()
	{
		return (int)(super.getSpeed() * turbo.getBoost());
	}

	public String toString()
	{
	  return getClass().getName();
	}

}

