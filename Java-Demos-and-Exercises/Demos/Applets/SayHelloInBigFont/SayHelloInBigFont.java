import java.applet.*;
import java.awt.*;

public class SayHelloInBigFont extends Applet
{

	public void paint(Graphics g)
	{
		Font f = new Font("Courier", Font.BOLD, 24);
		g.setFont(f);
		g.drawString("Hello again!", 5, 50);
	}

}
