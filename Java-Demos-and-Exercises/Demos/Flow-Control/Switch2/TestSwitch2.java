public class TestSwitch2
{

	public static void main(String[] args)
	{
		int weekDay = 0;

		if (args.length > 0)
			weekDay = Integer.parseInt(args[0]);

		switch (weekDay)
		{
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
				System.out.println("Go to work");
				break;

			case 6:
			case 7:
				System.out.println("Stay at home");
				break;

			default:
				System.out.println("Which planet are you on?");
				break;

		}
	}

}
