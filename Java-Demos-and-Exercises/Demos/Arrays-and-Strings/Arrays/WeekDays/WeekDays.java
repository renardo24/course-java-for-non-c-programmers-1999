public class WeekDays
{
	public static String parseWeekDay(int day)
	{
		String[] weekDays = {"Monday", "Tuesday", "Wednesday", "Thursday",
								"Friday", "Saturday", "Sunday"};

		String result = "";
		if ((day >= 0) && (day < weekDays.length))
			result = weekDays[day];
		return result;
	}

	public static void main(String[] args)
	{
		System.out.println("\nThe days of the week are:\n  ");
		for (int i = 0; i < 10; i++)
		{
			String day = parseWeekDay(i);
			if (i != 0 && day != "")
				System.out.print(", ");
			System.out.print(day);
		}
		System.out.println();
	}

}