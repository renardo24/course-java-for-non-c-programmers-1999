/* Class for employee */
 
public class Employee
{
	// Instance variables to hold employee's last name and age
	private String lastName;
    private int age;

	//
	// ToDo:
	//
	// Provide another instance variable to hold employee's first name



	//
	// Constructor that takes a String and an int argument
	// to initialise the instance variables for the employee's
	// last name and age, respectively.
	//
    public Employee(String ln, int a)
    {
		lastName = ln;
		age  = a;
	}


	//
	// ToDo:
	//
	// Provide a second constructor that takes two Strings and an int argument
	// to initialise the instance variables for the employee's
	// first name, last name and age respectively
	//





	//
	// ToDo:
	//
	// Modify the getName method so that it returns the employee's full name
	//
    public String getName()
    {
		return lastName;
	}

	
	//
	// This changeName method changes the employee's last name
	//
    public void changeName(String newLastName)
    {
		lastName = newLastName;
	}


	//
	// ToDo:
	//
	// Provide a second changeName method to change the employee's full name
	//






	//
	// The getAge method returns the employee's age
	//
    public int getAge()
    {
		return age;
	}


	//
	// The incAge method increments the employee's age
	//
    public void incAge()
    {
		// Do a sanity check
		if (age < 65)
			age++;
	}


}
