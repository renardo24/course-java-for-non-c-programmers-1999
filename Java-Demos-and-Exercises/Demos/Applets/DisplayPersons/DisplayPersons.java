import java.applet.*;
import java.awt.*;
import java.util.*;

public class DisplayPersons extends Applet
{
	Person[] persons = { 
		                  new Person("Smith", 28),
                          new Person("Jones", 25)
		               };
	Font font;

	public void paint(Graphics g)
	{
		font = new Font("Courier", Font.BOLD, 14);
		g.setFont(font);
		FontMetrics fm = g.getFontMetrics(font);
		int rowHeight = fm.getHeight();
		int colWidth = fm.getMaxAdvance() * 12;  // assume 12 chars max

		// Display column headings
		int x = 0, y = 0;
		g.drawString("Name", x, y += rowHeight);
		g.drawString("Age", x += colWidth, y);
		x = 0; y += rowHeight;

		// Display details of each person
		for (int i = 0; i < persons.length; i++)
		{
			x = 0;
			g.drawString(persons[i].getName(), x, y += rowHeight);
			g.drawString(persons[i].getAge(), x += colWidth, y);
		}

	}

}